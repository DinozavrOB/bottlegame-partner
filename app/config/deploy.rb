set :application, "Bottlegame Partners"
set :domain,      "dev.partners.bottle-game.com"
set :deploy_to,   "/srv/www/partners/com.bottle-game.partners"
set :app_path,    "app"
set :web_path,    "web"

set :deploy_via, :remote_cache
set :copy_exclude, [".git"]

set :repository,  "git@bitbucket.org:DinozavrOB/bottlegame-partner.git"
set :scm,         :git
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `subversion`, `mercurial`, `perforce`, or `none`

set :model_manager, "doctrine"
# Or: `propel`

role :web,        domain                         # Your HTTP server, Apache/etc
role :app,        domain, :primary => true       # This may be the same as your `Web` server

set  :keep_releases,  3

# Be more verbose by uncommenting the following line
logger.level = Logger::MAX_LEVEL

set :user, "malex"

set :group, "partners"
set :var_group, "bp_var"
set :branch, "develop"
set :group_writable, false
set :use_sudo, false
set :use_composer, true
set :cache_warmup, true

set :shared_files,          ["app/config/parameters.yml"]
set :webserver_user,        "www-data"
set :permission_method,     :acl
set :use_set_permissions,   true

set :shared_children,       [app_path + "/cache", app_path + "/logs", "vendor"]

ssh_options[:port] = "22"
ssh_options[:forward_agent] = false

default_run_options[:shell] = '/bin/bash'

desc "Change group to bottle"
task :change_group_permission, :roles=>[ :app, :web ] do
    run "#{try_sudo} setfacl -R -m group:#{group}:rwX #{deploy_to}/*"
end

desc "sudo test"
task :sudo_test, :roles=>[ :app, :web ] do
  run "#{try_sudo} whoami"
end

desc "share cache"
task :share_cache, :roles=>[ :app, :web ] do
    cache_dir = "/var/cache/www/com.bottle-game.partners"
    link = latest_release + '/' + app_path + '/cache'
    run "#{try_sudo} if [ -d #{link} ] ; then rm -rf #{link}; fi"
    try_sudo "mkdir -p #{cache_dir}"
#    try_sudo "setfacl -R -m user:#{webserver_user}:rwX,group:#{var_group}:rwX #{cache_dir}/*"
    try_sudo "ln -nfs #{cache_dir} #{link}"
end

desc "share logs"
task :share_logs, :roles=>[ :app, :web ] do
    logs_dir = "/var/log/www/com.bottle-game.partners"
    link = latest_release + '/' + app_path + '/logs'
    try_sudo "mkdir -p #{logs_dir}"
#    try_sudo "setfacl -m user:#{webserver_user}:rwX,group:#{var_group}:rwX #{logs_dir}/*"
    try_sudo "ln -nfs #{logs_dir} #{link}"
end

namespace :symfony do
    namespace :cache do
        [:clear, :warmup].each do |action|
            desc "Cache #{action.to_s}"
            task action, :roles => :app, :except => { :no_release => true } do
                case action
                when :clear
                  capifony_pretty_print "--> Clearing cache"
                when :warmup
                  capifony_pretty_print "--> Warming up cache"
                end

                run "#{try_sudo} sh -c 'cd #{latest_release} && #{php_bin} #{symfony_console} cache:#{action.to_s} #{console_options}'"
                if action == :warmup
                    run "#{try_sudo} setfacl -R -m user:#{webserver_user}:rwX,group:#{var_group}:rwX /var/cache/www/com.bottle-game.partners/*"
                end
                capifony_puts_ok
            end
        end
    end
end

before "symfony:composer:install", "share_cache"
before "symfony:composer:install", "share_logs"

after "deploy:create_symlink", "change_group_permission"
after "deploy:create_symlink", "symfony:assetic:dump"
