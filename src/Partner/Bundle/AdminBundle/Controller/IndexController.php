<?php

namespace Partner\Bundle\AdminBundle\Controller;

use Partner\Bundle\UserBundle\Form\Type\LoginFormType;
use Partner\Bundle\UserBundle\Form\Type\LostpassFormType;
use Partner\Bundle\UserBundle\Form\Type\RegistrationFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;

class IndexController extends Controller
{
    /**
     * @Template
     */
    public function indexAction()
    {
        $security = $this->container->get('security.context');
        if ($security->isGranted('ROLE_ADMIN') || $security->isGranted('ROLE_USER')) {
            return new RedirectResponse($this->container->get('router')->generate('sonata_admin_dashboard'));
        } else {
            return new RedirectResponse($this->container->get('router')->generate('fos_user_security_login'));
        }

    }
}

?>
