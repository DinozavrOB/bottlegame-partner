<?php

namespace Partner\Bundle\AdminBundle\Mailer;

use Partner\Bundle\DataBundle\Entity\Ticket;
use Partner\Bundle\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Routing\RouterInterface;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Mailer\MailerInterface;

class Mailer
{
    protected $mailer;
    protected $router;
    protected $templating;
    protected $parameters;

    protected $notificationEmail;
    protected $fromEmail;

    public function __construct($mailer, RouterInterface $router, EngineInterface $templating, array $parameters = array())
    {
        $this->mailer = $mailer;
        $this->router = $router;
        $this->templating = $templating;
        $this->parameters = $parameters;
    }

    public function sendAdminNotification(Ticket $ticket)
    {
        $subject = 'New ticket';
        $msg[] = "notification";
        $author = $ticket->getAuthor();
        $authorName = ($author instanceof User) ? (string)$author : 'unknown';
        $msg[] = "user " . $authorName . " create new ticket";
        $msg[] = $ticket->getSubject();
        $msg[] = $ticket->getDescription();

        $this->sendEmailMessage(
            $subject . "\n" . implode("\n", $msg),
            $this->fromEmail,
            $this->notificationEmail
        );
    }

    /**
     * @param string $renderedTemplate
     * @param $fromEmail
     * @param string $toEmail
     */
    protected function sendEmailMessage($renderedTemplate, $fromEmail, $toEmail)
    {
        // Render the email, use the first line as the subject, and the rest as the body
        $renderedLines = explode("\n", trim($renderedTemplate));
        $subject = $renderedLines[0];
        $body = implode("\n", array_slice($renderedLines, 1));

        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($fromEmail)
            ->setTo($toEmail)
            ->setBody($body);

        $this->mailer->send($message);
    }

    public function setNotificationEmail($email)
    {
        $this->notificationEmail = $email;
    }

    public function setFromEmail($email)
    {
        $this->fromEmail = $email;
    }
}