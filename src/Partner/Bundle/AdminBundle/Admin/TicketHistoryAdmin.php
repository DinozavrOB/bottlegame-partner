<?php

namespace Partner\Bundle\AdminBundle\Admin;

use Partner\Bundle\DataBundle\Entity\TicketHistory;
use Knp\Menu\ItemInterface;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Security\Core\SecurityContext;

class TicketHistoryAdmin extends Admin
{
    protected $parentAssociationMapping = 'ticket';

    protected function configureFormFields(FormMapper $form)
    {
        if (!$this->isChild()) {
            $form->add('ticket', 'sonata_type_model');
        }

        $form
            ->add('message');
    }

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('id')
            ->addIdentifier('ticket')
            ->addIdentifier('author')
            ->add('message')
            ->add('createdAt')
            ->add(
                '_action',
                'actions',
                array(
                    'actions' => array(
                        'view' => array(),
                        'edit' => array(),
                    )
                )
            );
    }

    public function getNewInstance()
    {
        /* @var $ticketHistory TicketHistory */
        $ticketHistory = parent::getNewInstance();
        $user = $this->getSecurityContext()->getToken()->getUser();
        $ticketHistory->setAuthor($user);

        return $ticketHistory;
    }

    protected function configureShowFields(\Sonata\AdminBundle\Show\ShowMapper $filter)
    {
        $filter
            ->add('id')
            ->add('author')
            ->add('ticket')
            ->add('message')
            ->add('createdAt');
    }

    /**
     *
     * @return SecurityContext
     */
    protected function getSecurityContext()
    {
        return $this->getConfigurationPool()->getContainer()->get('security.context');
    }

}