<?php

namespace Partner\Bundle\AdminBundle\Admin\AdminExtension;

use Sonata\AdminBundle\Admin\AdminExtension;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;

class SecurityAdminExtension extends AdminExtension
{
    public function configureQuery(AdminInterface $admin, ProxyQueryInterface $query, $context = 'list')
    {
        if (!$admin->isGranted('ROLE_ADMIN') && $context == 'list') {

        }
    }
}

?>