<?php

namespace Partner\Bundle\AdminBundle\Admin;

use Partner\Bundle\DataBundle\Entity\Site;
use Knp\Menu\ItemInterface;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Security\Core\SecurityContext;

class SiteAdmin extends Admin
{
    protected $baseRouteName = 'site';
    protected $baseRoutePattern = 'site';

    protected function configureShowFields(ShowMapper $filter)
    {
        $filter
            ->add('id')
            ->add('name')
            ->add('visits')
            ->add('link')
            ->add('percent')
            ->add('ratio');
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('name')
            ->add('visits')
            ->add('link')
            ->add('status', 'choice', array('choices' => Site::getStatusChoices()))
            ->add('percent', 'text')
            ->add('ratio', 'text');
    }

    public function getNewInstance()
    {
        $site = parent::getNewInstance();
        $user = $this->getSecurityContext()->getToken()->getUser();
        $site->setUser($user);

        return $site;
    }

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('id')
            ->add('name')
            ->add('visits')
            ->add('link')
            ->add('user')
            ->add('updatedAt')
            ->add(
                '_action',
                'actions',
                array(
                    'actions' => array(
                        'view' => array(),
                        'edit' => array(),
                    )
                )
            );
    }

    protected function configureSideMenu(ItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, array('edit', 'show'))) {
            return;
        }

        /* @var $admin Admin */
        $admin = $this->isChild() ? $this->getParent() : $this;

        $id = $admin->getRequest()->get('id');

        $menu->addChild(
            'Приложения',
            array('uri' => $admin->generateUrl('partner.admin.site_app.list', array('id' => $id)))
        );
    }

    /**
     *
     * @return SecurityContext
     */
    protected function getSecurityContext()
    {
        return $this->getConfigurationPool()->getContainer()->get('security.context');
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);

        if (!$this->isGranted('ROLE_ADMIN')) {
            /* @var $qb \Doctrine\ORM\QueryBuilder */
            $qb = $query->getQueryBuilder();
            $user = $this->getSecurityContext()->getToken()->getUser();
            $alias = $qb->getRootAlias();
            $qb->andWhere($alias . '.user = :user')->setParameter('user', $user);
        }

        return $query;
    }
}