<?php

namespace Partner\Bundle\AdminBundle\Admin;

use Partner\Bundle\DataBundle\Entity\Ticket;
use Knp\Menu\ItemInterface;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Security\Core\SecurityContext;

class TicketAdmin extends Admin
{
    protected $baseRouteName = 'ticket';
    protected $baseRoutePattern = 'ticket';

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        parent::configureDatagridFilters($filter);
    }

    public function getNewInstance()
    {
        /* @var $ticket Ticket */
        $ticket = parent::getNewInstance();
        $user = $this->getSecurityContext()->getToken()->getUser();
        $ticket->setAuthor($user);

        return $ticket;
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('site', null, array('required' => false))
            ->add('subject', 'sonata_type_model')
            ->add('description')
            ->add('status', 'choice', array('choices' => Ticket::getStatusChoices()));
    }

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('id')
//            ->add('subject')
            ->add('description')
            ->add(
                'site',
                null,
                array(
                    'route' => array(
                        'name' => 'show'
                    )
                )
            )
            ->add(
                'author',
                null,
                array(
                    'route' => array(
                        'name' => 'show'
                    )
                )
            )
            ->add('status')
            ->add(
                '_action',
                'actions',
                array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                    )
                )
            );
    }

    protected function configureSideMenu(ItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, array('edit', 'show'))) {
            return;
        }

        /* @var $admin Admin */
        $admin = $this->isChild() ? $this->getParent() : $this;

        $id = $admin->getRequest()->get('id');

        $url = $admin->generateUrl('partner.admin.ticket_history.list', array('id' => $id));

        $menu->addChild(
            'История сообщений',
            array('uri' => $admin->generateUrl('partner.admin.ticket_history.list', array('id' => $id)))
        );
    }

    protected function configureShowFields(ShowMapper $filter)
    {
        $filter
            ->add('id')
            ->add('subject')
            ->add('description')
            ->add('author')
            ->add('status')
            ->add('site');
    }

    /**
     *
     * @return SecurityContext
     */
    protected function getSecurityContext()
    {
        return $this->getConfigurationPool()->getContainer()->get('security.context');
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);

        if (!$this->isGranted('ROLE_ADMIN')) {
            /* @var $qb \Doctrine\ORM\QueryBuilder */
            $qb = $query->getQueryBuilder();
            $user = $this->getSecurityContext()->getToken()->getUser();
            $alias = $qb->getRootAliases()[0];
            $qb->andWhere($alias . '.user = :user')->setParameter('user', $user);
        }

        return $query;
    }

}