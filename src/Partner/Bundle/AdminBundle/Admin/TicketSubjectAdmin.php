<?php

namespace Partner\Bundle\AdminBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class TicketSubjectAdmin extends Admin
{
    protected $baseRouteName = 'ticketsubject';
    protected $baseRoutePattern = 'ticketsubject';

    public function configureFormFields(FormMapper $form)
    {
        $form
            ->add('title', null, array('label' => 'Название'))
            ->add('active', 'sonata_type_boolean', array('label' => 'Активнен', 'transform' => true))
            ->add('selectable', 'sonata_type_boolean', array('label' => 'Возможность выбора', 'transform' => true))
            ->add(
                'moderateRequest',
                'sonata_type_boolean',
                array('label' => 'Это запрос на модерацию сайта', 'transform' => true)
            )
            ->add('defaultDescription', 'text', array('required' => false));
    }

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('id')
            ->add('title')
            ->add('selectable')
            ->add('moderateRequest');
    }


} 