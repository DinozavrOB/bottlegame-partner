<?php

namespace Partner\Bundle\AdminBundle\Admin;

use Knp\Menu\ItemInterface;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class SettingAdmin extends Admin
{
    protected $parentAssociationMapping = 'group';

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('name')
            ->add('systemName')
            ->add(
                'type',
                'choice',
                array('choices' => array('string' => 'Текст', 'array' => 'Массив', 'boolean' => 'Да/Нет'))
            )
            ->add(
                'availableValues',
                'sonata_type_collection',
                array('by_reference' => false),
                array(
                    'edit' => 'inline',
                    'inline' => 'table',
                )
            )
            ->add('defaultValue', 'text', array('required' => false))
            ->add('group', 'sonata_type_model')
            ->add('active', 'sonata_type_boolean')
            ->add('visible', 'sonata_type_boolean')
            ->add('editable', 'sonata_type_boolean')
            ->add('description');
    }

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('id')
            ->add('name')
            ->add('description')
            ->add('group.name')
            ->add('visible', null, array('editable' => true))
            ->add('editable', null, array('editable' => true))
            ->add('active', null, array('editable' => true))
            ->add(
                '_action',
                'actions',
                array(
                    'actions' => array(
                        'view' => array(),
                        'edit' => array(),
                    )
                )
            );
    }

    protected function configureShowFields(ShowMapper $filter)
    {
        $filter
            ->add('name')
            ->add('group.name')
            ->add('description')
            ->add('defaultValue');
    }

}