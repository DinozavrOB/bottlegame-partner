<?php

namespace Partner\Bundle\AdminBundle\Admin;

use Partner\Bundle\DataBundle\Entity\Setting;
use Partner\Bundle\DataBundle\Entity\SettingGroup;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Knp\Menu\ItemInterface;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Security\Core\SecurityContext;

class SiteAppAdmin extends Admin
{
    protected $parentAssociationMapping = 'site';

    protected function configureShowFields(ShowMapper $showMapper)
    {
        /* @var $subject SiteApp */
        $subject = $this->getSubject();

        if (!$subject->getId()) {
            return;
        }

        $showMapper
            ->with('Приложение')
            ->add('app.name', null, array('label' => 'Название'))
            ->end();

        $groupSettings = array();
        $appSettings = clone ($subject->getApp()->getSettings());
        $groupSort = array();
        /* @var $setting Setting */
        foreach ($appSettings as $index => $setting) {
            $group = $setting->getGroup();
            if ($group instanceOf SettingGroup) {
                $groupName = $group->getName();
                $groupSettings[$groupName][] = $setting;
                $groupSort[$group->getSort()] = $group->getName();
                unset($appSettings[$index]);
            }
        }

        ksort($groupSort);

        $showMapper->with('Настройки');
        foreach ($groupSort as $groupName) {
            $showMapper->with($groupName);
            foreach ($groupSettings[$groupName] as $setting) {
                $this->addShowField($showMapper, $setting);
            }
            $showMapper->end();
        }

        foreach ($appSettings as $setting) {
            $this->addShowField($showMapper, $setting);
        }

        $showMapper->end();

    }

    protected function configureFormFields(FormMapper $form)
    {
        if (!$this->isChild()) {
            return;
        }

        /* @var $subject SiteApp */
        $subject = $this->getSubject();
        if (!$subject->getId()) {
            $site = $subject->getSite();
            $currentApps = array();
            /* @var $siteApp SiteApp */
            foreach ($site->getApps() as $siteApp) {
                $currentApps[] = $siteApp->getApp();
            }

            $form
                ->add(
                    'app',
                    null,
                    array(
                        'query_builder' =>
                            function (EntityRepository $er) use ($currentApps) {
                                $qb = $er->createQueryBuilder('sa');
                                if (!empty($currentApps)) {
                                    $qb->where('sa.id not in (:apps)')->setParameter('apps', $currentApps);
                                }

                                return $qb;
                            },
                        'required' => true
                    )
                );
        } else {
            $groupSettings = array();
            $appSettings = clone ($subject->getApp()->getSettings());
            $groupSort = array();
            /* @var $setting Setting */
            foreach ($appSettings as $index => $setting) {
                $group = $setting->getGroup();
                if ($group instanceOf SettingGroup) {
                    $groupName = $group->getName();
                    $groupSettings[$groupName][] = $setting;
                    $groupSort[$group->getSort()] = $group->getName();
                    unset($appSettings[$index]);
                }
            }

            ksort($groupSort);

            foreach ($groupSort as $groupName) {
                $form->with($groupName);
                foreach ($groupSettings[$groupName] as $setting) {
                    $this->addFormChild($form, $setting);
                }
                $form->end();
            }

            foreach ($appSettings as $setting) {
                $this->addFormChild($form, $setting);
            }
        }
    }

    private function addFormChild(FormMapper $form, Setting $setting)
    {
        switch ($setting->getType()):
            case 'array':
                $form->add(
                    $setting->getSlug(),
                    'choice',
                    array('choices' => $setting->getAvailableValuesChoices(), 'label' => $setting->getName())
                );
                break;
            case 'boolean':
                $form->add($setting->getSlug(), 'sonata_type_boolean', array('label' => $setting->getName()));
                break;
            default:
                $form->add($setting->getSlug(), 'text', array('label' => $setting->getName()));
        endswitch;
    }

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('id')
            ->add('app')
            ->add(
                '_action',
                'actions',
                array(
                    'actions' => array(
                        'view' => array(),
                        'edit' => array(),
                    )
                )
            );
    }

    /**
     *
     * @return SecurityContext
     */
    protected function getSecurityContext()
    {
        return $this->getConfigurationPool()->getContainer()->get('security.context');
    }

    public function addShowField(ShowMapper $showMapper, Setting $setting)
    {
        $options = array('label' => $setting->getName());
        $type = $setting->getType();
        $slug = $setting->getSlug();
        if ($setting->getType() === 'array') {
            $options['template'] = 'PartnerAdminBundle:Field:array.html.twig';
            $type = 'text';
            $criteria = Criteria::create()->where(Criteria::expr()->eq('value', $this->getSubject()->$slug));
            $currentSetting = $setting->getAvailableValues()->matching($criteria)->first();
            $options['setting'] = $currentSetting;
        }
        $showMapper->add($slug, $type, $options);
    }

}