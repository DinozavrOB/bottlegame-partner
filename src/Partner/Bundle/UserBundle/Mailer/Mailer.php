<?php

namespace Partner\Bundle\UserBundle\Mailer;

use FOS\UserBundle\Mailer\Mailer as BaseMailer;
use Symfony\Component\Security\Core\User\UserInterface;

class Mailer extends BaseMailer
{
    public function sendSuccessfulRegistrationMessage(UserInterface $user)
    {
        $template = $this->parameters['success_registration.template'];
        $url = $this->router->generate('partner_frontend_homepage', array(), true);
        $rendered = $this->templating->render(
            $template,
            array(
                'user' => $user,
                'url' => $url
            )
        );

        $this->sendEmailMessage($rendered, $this->parameters['from_email']['confirmation'], $user->getEmail());
    }
}

?>