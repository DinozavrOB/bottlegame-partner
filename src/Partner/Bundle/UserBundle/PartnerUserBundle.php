<?php

namespace Partner\Bundle\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class PartnerUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
