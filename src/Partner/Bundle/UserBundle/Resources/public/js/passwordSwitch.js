$(function () {
    $('.change-password').click(function (e) {
        e.preventDefault();
        var $inputField = $(this).parent().find('input'),
            currentType = $inputField.attr('type'),
            newType = (currentType === 'text') ? 'password' : 'text';

        $inputField.attr('type', newType);
        $(this).toggleClass('active');
    });
});