<?php

namespace Partner\Bundle\UserBundle\Controller;

use Partner\Bundle\UserBundle\Form\Type\LostpassFormType;
use FOS\UserBundle\Controller\SecurityController as BaseController;
use FOS\UserBundle\Form\Type\RegistrationFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * Description of SecurityController
 *
 * @author shatun
 */
class SecurityController extends BaseController
{

    /**
     * @Template
     */
    public function loginAction()
    {
        $request = $this->container->get('request');
        /* @var $request Request */
        $session = $request->getSession();
        /* @var $session Session */

        // get the error if any (works with forward and redirect -- see below)
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } elseif (null !== $session && $session->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = '';
        }

        if ($error) {
            // TODO: this is a potential security risk (see http://trac.symfony-project.org/ticket/9523)
            $error = $error->getMessage();
        }
        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get(SecurityContext::LAST_USERNAME);

        if ($this->container->get('security.context')->isGranted('ROLE_USER')) {
            $refererUri = $request->server->get('HTTP_REFERER');

            return new RedirectResponse($refererUri && $refererUri != $request->getUri(
            ) ? $refererUri : $this->container->get('router')->generate('partner_frontend_homepage'));
        }

        return [
            'last_username' => $lastUsername,
            'error' => $error,
        ];
    }

}

?>