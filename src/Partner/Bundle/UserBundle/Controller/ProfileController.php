<?php

namespace Partner\Bundle\UserBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\UserBundle\Doctrine\UserManager;
use Partner\Bundle\UserBundle\Form\Type\ProfileFormType;

/**
 * Description of UserController
 *
 * @author shatun
 */
class ProfileController extends FOSRestController
{
    protected $userManager;

    public function getAction()
    {
        $user = $this->getUser();

        if (!$this->getRequest()->isXmlHttpRequest()) {
            return $this->forward('PartnerFrontendBundle:Index:index');
        }

        $view = $this
            ->view($user)
            ->setFormat('json');

        return $this->handleView($view);
    }

    public function editAction()
    {
        /* @var $userManager UserManager */
        $user = $this->getUser();
        $data = $this->getRequest()->request->all();
        $changePassword = !empty($data['cur_password']) ? true : false;
        $form = $this->createForm(new ProfileFormType($changePassword), $user);
        $children = $form->all();
        $submittedData = array_intersect_key($data, $children);
        $form->submit($submittedData, false);

        $view = $this->view();
        if ($form->isValid()) {
            $this->getUserManager()->updateUser($user);
            $view->setStatusCode(204);
        } else {
            $view
                ->setData($form)
                ->setStatusCode(400);
        }

        $view->setFormat('json');

        return $this->handleView($view);
    }

    public function changePasswordAction()
    {

    }

    /**
     *
     * @return UserManager
     */
    protected function getUserManager()
    {
        if (!$this->userManager) {
            $this->userManager = $this->get('fos_user.user_manager');
        }

        return $this->userManager;
    }

}

?>