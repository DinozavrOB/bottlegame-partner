<?php

namespace Partner\Bundle\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('PartnerUserBundle:Default:index.html.twig', array('name' => $name));
    }
}
