<?php

namespace Partner\Bundle\UserBundle\Controller;

use FOS\UserBundle\Controller\ResettingController as BaseController;
use FOS\UserBundle\Model\UserInterface;

/**
 * Description of ResettingController
 *
 * @author shatun
 */
class ResettingController extends BaseController
{
    /**
     * Request reset user password: show form
     */
    public function requestAction()
    {
        return $this->container->get('templating')
            ->renderResponse(
                'FOSUserBundle:Resetting:request.html.' . $this->getEngine()
            );
    }

    protected function getRedirectionUrl(UserInterface $user)
    {
        return $this->container->get('router')->generate('partner_frontend_homepage');
    }
}

?>