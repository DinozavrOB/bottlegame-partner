<?php

namespace Partner\Bundle\UserBundle\Entity;

use Sonata\UserBundle\Entity\BaseUser as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="`user`")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="Group", inversedBy="users")
     * @ORM\JoinTable(name="user_user_group",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     * )
     */
    protected $groups;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     * @var type
     */
    protected $type;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     * @var type
     */
    protected $skype;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     * @var type
     */
    protected $fullname;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var type
     */
    protected $comment;

    /**
     * @ORM\OneToMany(targetEntity="Partner\Bundle\DataBundle\Entity\Ticket", mappedBy="author")
     * @var type
     */
    protected $tickets;

    /**
     *
     * @ORM\OneToMany(targetEntity="Partner\Bundle\DataBundle\Entity\Site", mappedBy="user")
     */
    protected $sites;

    public function __construct()
    {
        parent::__construct();
        $this->groups = new ArrayCollection();
        $this->tickets = new ArrayCollection();
        $this->sites = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return User
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set fullname
     *
     * @param string $fullname
     * @return User
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;

        return $this;
    }

    /**
     * Get fullname
     *
     * @return string
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return User
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set skype
     *
     * @param string $skype
     * @return User
     */
    public function setSkype($skype)
    {
        $this->skype = $skype;

        return $this;
    }

    /**
     * Get skype
     *
     * @return string
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * Add tickets
     *
     * @param \Partner\Bundle\DataBundle\Entity\Ticket $tickets
     * @return User
     */
    public function addTicket(\Partner\Bundle\DataBundle\Entity\Ticket $tickets)
    {
        $this->tickets[] = $tickets;

        return $this;
    }

    /**
     * Remove tickets
     *
     * @param \Partner\Bundle\DataBundle\Entity\Ticket $tickets
     */
    public function removeTicket(\Partner\Bundle\DataBundle\Entity\Ticket $tickets)
    {
        $this->tickets->removeElement($tickets);
    }

    /**
     * Get tickets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTickets()
    {
        return $this->tickets;
    }

    /**
     * Add sites
     *
     * @param \Partner\Bundle\DataBundle\Entity\Site $sites
     * @return User
     */
    public function addSite(\Partner\Bundle\DataBundle\Entity\Site $sites)
    {
        $this->sites[] = $sites;

        return $this;
    }

    /**
     * Remove sites
     *
     * @param \Partner\Bundle\DataBundle\Entity\Site $sites
     */
    public function removeSite(\Partner\Bundle\DataBundle\Entity\Site $sites)
    {
        $this->sites->removeElement($sites);
    }

    /**
     * Get sites
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSites()
    {
        return $this->sites;
    }
}