<?php

namespace Partner\Bundle\UserBundle\Security\Generator;

use Symfony\Component\Security\Core\Util\SecureRandomInterface;

class PasswordGenerator
{
    /**
     *
     * @var SecureRandomInterface
     */
    protected $generator;

    public function __construct(SecureRandomInterface $generator)
    {
        $this->generator = $generator;
    }

    public function generate($length)
    {
        if (empty($length)) {
            return '';
        }

        $hash = $this->getHash();

        return substr($hash, 0, $length);
    }

    protected function getHash()
    {
        $bytes = $this->generator->nextBytes(32);

        $hash = hash('sha512', $bytes);

        return $hash;
    }
}

?>