<?php

namespace Partner\Bundle\UserBundle\Event;

use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\EventDispatcher\Event;

class SuccessRegisterEvent extends Event
{
    /**
     * @var UserInterface
     */
    protected $user;

    public function setUser(UserInterface $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }
}

?>