<?php

namespace Partner\Bundle\UserBundle\Event\Listener;

use Partner\Bundle\DataBundle\Entity\Ticket;
use Partner\Bundle\UserBundle\Event\SuccessRegisterEvent;
use Partner\Bundle\UserBundle\Mailer\Mailer;
use Doctrine\Bundle\DoctrineBundle\Registry;

class SuccessRegisterListener
{
    protected $mailer;

    /**
     *
     * @var Registry
     */
    protected $doctrine;

    public function __construct(Mailer $mailer, Registry $doctrine)
    {
        $this->mailer = $mailer;
        $this->doctrine = $doctrine;
    }

    public function onSuccessRegisterEvent(SuccessRegisterEvent $event)
    {
        $user = $event->getUser();
        if (!$user->isEnabled()) {
            return false;
        }

        $this->mailer->sendSuccessfulRegistrationMessage($user);
    }
}

?>