<?php

namespace Partner\Bundle\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use \Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RegistrationFormType extends AbstractType
{

    private $class;

    /**
     * @param string $class The User class name
     */
    public function __construct($class)
    {
        $this->class = $class;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', null, array('label' => 'Логин', 'attr' => array('tabindex' => 1)))
            ->add(
                'password',
                'password',
                array('label' => 'Пароль:', 'property_path' => 'plainPassword', 'attr' => array('tabindex' => 3))
            )
            ->add(
                'type',
                'choice',
                array(
                    'choices' => array(
                        'person' => 'Частное лицо',
                        'organization' => 'Организация'
                    ),
                    'expanded' => true,
                    'multiple' => false,
                    'attr' => array('tabindex' => 4)
                )
            )
            ->add('firstname', null, array('label' => 'Имя:', 'attr' => array('tabindex' => 6)))
            ->add('lastname', null, array('label' => 'Фамилия:', 'attr' => array('tabindex' => 7)))
            ->add('email', 'email', array('label' => 'E-mail:', 'attr' => array('tabindex' => 2)))
//            ->add('agree', 'checkbox', array('required'=>true, 'mapped' => false))
            ->add(
                'captcha',
                'captcha',
                array(
                    'width' => 100,
                    'height' => 25,
                    'length' => 5,
                    'charset' => '1234567890',
                    'reload' => true,
                    'as_url' => true,
                    'attr' => array('class' => 'field-while', 'tabindex' => 5)
                )
            );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => $this->class,
            )
        );
    }

    public function getName()
    {
        return 'partner_user_registration';
    }

}

?>
