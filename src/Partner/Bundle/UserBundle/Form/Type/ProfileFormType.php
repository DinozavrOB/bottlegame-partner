<?php

namespace Partner\Bundle\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

class ProfileFormType extends AbstractType
{
    protected $changePassword;

    public function __construct($changePassword = false)
    {
        $this->changePassword = $changePassword;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('email')
            ->add('firstname')
            ->add('lastname');

        if ($this->changePassword) {
            $builder
                ->add(
                    'cur_password',
                    'password',
                    array(
                        'required' => true,
                        'label' => 'Current password',
                        'mapped' => false,
                        'constraints' => new UserPassword()
                    )
                )
                ->add('plainPassword', 'password', array('required' => true));
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Partner\Bundle\UserBundle\Entity\User',
                'csrf_protection' => false
            )
        );
    }

    public function getName()
    {
        return 'user';
    }
}

?>