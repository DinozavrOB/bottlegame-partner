<?php

namespace Partner\Bundle\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LostpassFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'email', array('label' => 'E-mail'))
            ->add(
                'captcha',
                'captcha',
                array(
                    'width' => 100,
                    'height' => 25,
                    'length' => 5,
                    'charset' => '1234567890'
                )
            );
    }

    public function getName()
    {
        return 'lostpass';
    }
}

?>
