<?php

namespace Partner\Bundle\UserBundle\Form\Handler;

use Partner\Bundle\UserBundle\Entity\Group;
use Partner\Bundle\UserBundle\Event\SuccessRegisterEvent;
use Partner\Bundle\UserBundle\Security\Generator\PasswordGenerator;
use FOS\UserBundle\Form\Handler\RegistrationFormHandler as BaseHandler;
use FOS\UserBundle\Mailer\MailerInterface;
use FOS\UserBundle\Model\GroupManagerInterface;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

class RegistrationFormHandler extends BaseHandler
{
    protected $request;
    protected $userManager;
    protected $form;
    protected $mailer;
    protected $tokenGenerator;

    /**
     *
     * @var PasswordGenerator
     */
    protected $passwordGenerator;

    /**
     *
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     *
     * @var GroupManagerInterface
     */
    protected $groupManager;

    /**
     * Default group name for new users
     * @var string
     */
    protected $defaultGroupName;

    public function __construct(
        FormInterface $form,
        Request $request,
        UserManagerInterface $userManager,
        MailerInterface $mailer,
        TokenGeneratorInterface $tokenGenerator
    ) {
        $this->form = $form;
        $this->request = $request;
        $this->userManager = $userManager;
        $this->mailer = $mailer;
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * @param \FOS\UserBundle\Model\UserInterface $user
     * @param boolean $confirmation
     * @throws \Symfony\Component\Routing\Exception\ResourceNotFoundException
     */
    protected function onSuccess(UserInterface $user, $confirmation)
    {
//        $user->setUsername($user->getEmail());
//        $user->setPlainPassword($this->passwordGenerator->generate(10));
        $group = $this->getGroupManager()->findGroupByName($this->defaultGroupName);

        if (!$group instanceof Group) {
            throw new ResourceNotFoundException('default group not found');
        }

        $user->addGroup($group);

        if ($confirmation) {
            $user->setEnabled(false);
            if (null === $user->getConfirmationToken()) {
                $user->setConfirmationToken($this->tokenGenerator->generateToken());
            }

            $this->mailer->sendConfirmationEmailMessage($user);
        } else {
            $user->setEnabled(true);
        }

        $this->userManager->updateUser($user);

        $event = new SuccessRegisterEvent();
        $event->setUser($user);
        $this->dispatcher->dispatch('partner_user.events.success_register', $event);
    }

    public function setEventDispatcher(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function setPasswordGenerator(PasswordGenerator $generator)
    {
        $this->passwordGenerator = $generator;
    }

    public function setGroupManager(GroupManagerInterface $groupManager)
    {
        $this->groupManager = $groupManager;
    }

    public function getGroupManager()
    {
        return $this->groupManager;
    }

    public function setDefaultGroupName($groupName)
    {
        $this->defaultGroupName = $groupName;
    }
}
