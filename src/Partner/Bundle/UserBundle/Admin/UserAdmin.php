<?php

namespace Partner\Bundle\UserBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\UserBundle\Admin\Entity\UserAdmin as BaseUserAdmin;
use Sonata\UserBundle\Model\UserInterface;

class UserAdmin extends BaseUserAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        if ($this->isGranted('ROLE_ADMIN')) {
            parent::configureFormFields($formMapper);

            return;
        }

        $formMapper
            ->with('General')
                ->add('username')
                ->add('email')
                ->add('plainPassword', 'text', array('required' => false))
            ->end()
            ->with('Profile')
                ->add('dateOfBirth', 'birthday', array('required' => false))
                ->add('firstname', null, array('required' => false))
                ->add('lastname', null, array('required' => false))
                ->add('website', 'url', array('required' => false))
                ->add('biography', 'text', array('required' => false))
                ->add(
                    'gender',
                    'choice',
                    array(
                        'choices' => array(
                            UserInterface::GENDER_UNKNOWN => 'gender_unknown',
                            UserInterface::GENDER_FEMALE => 'gender_female',
                            UserInterface::GENDER_MAN => 'gender_male',
                        ),
                        'required' => true,
                        'translation_domain' => $this->getTranslationDomain()
                    )
                )
                ->add('locale', 'locale', array('required' => false))
                ->add('timezone', 'timezone', array('required' => false))
                ->add('phone', null, array('required' => false))
            ->end()
            ->with('Social')
                ->add('facebookUid', null, array('required' => false))
                ->add('facebookName', null, array('required' => false))
                ->add('twitterUid', null, array('required' => false))
                ->add('twitterName', null, array('required' => false))
                ->add('gplusUid', null, array('required' => false))
                ->add('gplusName', null, array('required' => false))
            ->end();
    }
}
