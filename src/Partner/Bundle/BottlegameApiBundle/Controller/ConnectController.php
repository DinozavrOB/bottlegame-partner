<?php
/**
 * Created by PhpStorm.
 * User: shatun
 * Date: 11/14/13
 * Time: 4:05 PM
 */

namespace Partner\Bundle\BottlegameApiBundle\Controller;


use Partner\Bundle\BottlegameApiBundle\OAuth\ResourceOwner\BottlegameResourceOwner;
use Partner\Bundle\BottlegameApiBundle\Service\BottlegameApiSdk;
use Partner\Bundle\DataBundle\Entity\Site;
use Partner\Bundle\DataBundle\Entity\SiteApp;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ConnectController extends Controller
{
    public function connectAction(Request $request)
    {
        if (!$this->container->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }

        /* @var $bottlegameApiOAuth BottlegameApiOAuth */
        $bottlegameApiOAuth = $this->container->get('bottegame_api.oauth');
        $redirectUri = $this->generateUrl('partner_bottlegame_api_connect', array(), true);

        if (!$bottlegameApiOAuth->connect($request, $redirectUri)) {
            throw new AccessDeniedException('access token not found');
        }

        return new RedirectResponse($this->generateUrl('sonata_admin_dashboard', array(), true));
    }

    public function loginAction()
    {
        if (!$this->container->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }

        /* @var $resourceOwner BottlegameResourceOwner */
        $resourceOwner = $this->container->get('hwi_oauth.resource_owner.bottlegame');

        $redirectUri = $resourceOwner->getAuthorizationUrl(
            $this->generateUrl('partner_bottlegame_api_connect', array(), true)
        );

        return new RedirectResponse($redirectUri);
    }
} 