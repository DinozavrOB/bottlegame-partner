<?php
/**
 * Created by PhpStorm.
 * User: shatun
 * Date: 11/13/13
 * Time: 4:24 PM
 */

namespace Partner\Bundle\BottlegameApiBundle\Admin\Block;

use Partner\Bundle\BottlegameApiBundle\Service\BottlegameApiOAuth;
use Sonata\BlockBundle\Block\BaseBlockService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\BlockBundle\Block\BlockContextInterface;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Validator\ErrorElement;

class ApiBlockService extends BaseBlockService
{
    /**
     * @var BottlegameApiOAuth
     */
    protected $apiOAuth;

    /**
     * @param ErrorElement $errorElement
     * @param BlockInterface $block
     *
     * @return void
     */
    public function validateBlock(ErrorElement $errorElement, BlockInterface $block)
    {
        // TODO: Implement validateBlock() method.
    }

    /**
     * @param FormMapper $form
     * @param BlockInterface $block
     *
     * @return void
     */
    public function buildEditForm(FormMapper $form, BlockInterface $block)
    {

    }

    public function setDefaultSettings(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'template' => 'PartnerBottlegameApiBundle:Block:api_user.html.twig',
            )
        );
    }

    public function setApiOAuth(BottlegameApiOAuth $apiOAuth)
    {
        $this->apiOAuth = $apiOAuth;
    }

    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $tokenLastRefreshTime = $this->apiOAuth->getTokenLastRefreshTime();

        return $this->renderResponse(
            $blockContext->getTemplate(),
            array(
                'block_context' => $blockContext,
                'block' => $blockContext->getBlock(),
                'last_token_refresh' => $tokenLastRefreshTime
            ),
            $response
        );
    }

} 