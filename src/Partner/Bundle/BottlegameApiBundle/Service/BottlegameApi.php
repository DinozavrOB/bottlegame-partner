<?php
/**
 * Created by PhpStorm.
 * User: shatun
 * Date: 11/19/13
 * Time: 4:48 PM
 */

namespace Partner\Bundle\BottlegameApiBundle\Service;


use Buzz\Message\MessageInterface;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Partner\Bundle\DataBundle\Entity\Site;
use Partner\Bundle\DataBundle\Entity\SiteApp;

class BottlegameApi
{
    /**
     * @var BottlegameApiSdk
     */
    protected $apiSdk;

    /**
     * @var SettingsTransformer
     */
    protected $settingsTransformer;

    /**
     * Array with last error, contains code and message keys
     * @var array
     */
    protected $error = array('code' => '', 'message' => '');

    function __construct(BottlegameApiSdk $apiSdk, SettingsTransformer $settingsTransformer)
    {
        $this->apiSdk = $apiSdk;
        $this->settingsTransformer = $settingsTransformer;
    }

    public function createSite(Site $site)
    {
        $data = $this->prepareData($site);
        $response = $this->apiSdk->createPartner($data);

        return $this->validateResponse($response);
    }

    public function updateSite(Site $site)
    {
        $data = $this->prepareData($site);
        $response = $this->apiSdk->editPartnerData($site->getSlug(), $data);

        return $this->validateResponse($response);
    }

    public function deleteSite(Site $site)
    {
        $response = $this->apiSdk->deletePartner($site->getSlug());

        return $this->validateResponse($response);
    }

    public function getSite($slug)
    {
        $response = $this->apiSdk->getPartnerData($slug);

        if (!$this->validateResponse($response)) {
            return false;
        }

        return $response->getContent();
    }

    public function getAppUrl(SiteApp $siteapp)
    {
        $slug = $siteapp->getSite()->getSlug();
        $systemName = $siteapp->getApp()->getSystemName();
        $response = $this->apiSdk->getApplicationUrls($slug, $systemName);

        if (!$this->validateResponse($response)) {
            return false;
        }

        $content = json_decode($response->getContent(), true);

        return isset($content[$systemName]) ? $content[$systemName] : null;
    }

    protected function prepareData(Site $site)
    {
        $data = array(
            'name' => $site->getName(),
            'slug' => $site->getSlug(),
            'percent' => $site->getPercent(),
            'ratio' => $site->getRatio(),
        );

        $settings = $site->getApps()->first()->getSettings();

        $data['settings'] = $this->settingsTransformer->transform($settings);

        return $data;
    }

    protected function validateResponse(MessageInterface $response)
    {
        $content = json_decode($response->getContent(), true);
        if (isset($content['code'])) {
            $message = isset($content['message']) ? $content['message'] : null;
            $this->setError($content['code'], $message);

            return false;
        }

        return true;
    }

    protected function setError($code, $message)
    {
        $this->error = array('code' => $code, 'message' => $message);
    }

    public function getLastError()
    {
        return $this->error;
    }
}