<?php
/**
 * Created by PhpStorm.
 * User: shatun
 * Date: 11/14/13
 * Time: 5:45 PM
 */

namespace Partner\Bundle\BottlegameApiBundle\Service;


use Buzz\Browser;
use Buzz\Message\MessageInterface;
use Partner\Bundle\DataBundle\Entity\Site;

class BottlegameApiSdk
{
    /**
     * @var BottlegameApiOAuth
     */
    protected $accessToken;

    protected $browser;

    protected $apiOAuth;

    protected $api = 'http://api.partner/api/';

    function __construct(BottlegameApiOAuth $apiOAuth, Browser $browser, $api_host)
    {
        $this->browser = $browser;
        $this->apiOAuth = $apiOAuth;
        $this->api = $api_host . '/api';
    }

    public function getPartnerData($slug)
    {
        $url = $this->getApiUrl('partner/' . $slug);
        $response = $this->browser->get($url);

        return $response;
    }

    public function editPartnerData($slug, $data)
    {
        $url = $this->getApiUrl('partner/' . $slug);
        $response = $this->browser->put($url, array('Content-Type: application/json'), json_encode($data));

        return $response;
    }

    public function createPartner($data)
    {
        $url = $this->getApiUrl('partner/');
        $response = $this->browser->post($url, array('Content-Type: application/json'), json_encode($data));

        return $response;
    }

    public function deletePartner($slug)
    {
        $url = $this->getApiUrl('partner/' . $slug);
        $response = $this->browser->delete($url, array('Content-Type: application/json'));

        return $response;
    }

    /**
     * @param $slug
     * @param null $appName
     * @return MessageInterface
     */
    public function getApplicationUrls($slug, $appName = null)
    {
        $action = sprintf('partner/%s/app_url/%s', $slug, $appName);
        $url = $this->getApiUrl($action);
        $response = $this->browser->get($url);

        return $response;
    }

    protected function getApiUrl($action)
    {
        return sprintf('%s/%s?access_token=%s', $this->api, $action, $this->apiOAuth->getAccessToken());
    }
}