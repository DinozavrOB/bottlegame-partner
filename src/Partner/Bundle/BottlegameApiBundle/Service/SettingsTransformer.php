<?php
/**
 * Created by PhpStorm.
 * User: shatun
 * Date: 11/19/13
 * Time: 4:48 PM
 */

namespace Partner\Bundle\BottlegameApiBundle\Service;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Partner\Bundle\DataBundle\Entity\Setting;

class SettingsTransformer
{
    protected $doctrine;

    function __construct(Registry $doctrine, $entityName)
    {
        $this->doctrine = $doctrine;
        $this->entityName = $entityName;
    }

    public function transform(array $settings)
    {
        if (empty($settings)) {
            return array();
        }

        $allSettings = $this->findAllSettings('slug');
        $result = array();
        foreach ($settings as $key => $value) {
            if (!isset($allSettings[$key])) {
                continue;
            }

            /* @var $setting Setting */
            $setting = $allSettings[$key];
            $result[$setting->getSystemName()] = $value;
        }

        return $result;
    }

    public function reverseTransform(array $apiSettings)
    {
        if (empty($apiSettings)) {
            return $apiSettings;
        }

        $allSettings = $this->findAllSettings('systemName');
        $result = array();
        foreach ($apiSettings as $key => $value) {
            if (!isset($allSettings['$key'])) {
                continue;
            }

            /* @var $setting Setting */
            $setting = $allSettings[$key];
            $result[$setting->getSlug()] = $value;
        }

        return $result;
    }

    protected function findAllSettings($indexBy)
    {
        return $this->doctrine->getRepository($this->entityName)
            ->fetchAll($indexBy);
    }
}