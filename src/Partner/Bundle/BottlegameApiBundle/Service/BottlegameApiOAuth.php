<?php
/**
 * Created by PhpStorm.
 * User: shatun
 * Date: 11/14/13
 * Time: 5:44 PM
 */

namespace Partner\Bundle\BottlegameApiBundle\Service;

use HWI\Bundle\OAuthBundle\OAuth\ResourceOwnerInterface;
use \Predis\Client as Redis;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\TokenNotFoundException;

/**
 * Class BottlegameApiOAuth
 * @package Partner\Bundle\BottlegameApiBundle\Service
 */
class BottlegameApiOAuth
{
    const TOKEN_LIFETIME = 2592000; //60*60*24*30
    /**
     * @var \Predis\Client
     */
    protected $redis;

    /**
     * @var \HWI\Bundle\OAuthBundle\OAuth\ResourceOwnerInterface
     */
    protected $resourceOwner;

    /**
     * @var Array<'access_token', 'token_type', 'expires_in', 'refresh_token'>
     */
    protected $token;

    function __construct(Redis $redis, ResourceOwnerInterface $resourceOwner)
    {
        $this->redis = $redis;
        $this->resourceOwner = $resourceOwner;
    }

    /**
     * @param Request $request
     * @param string $redirectUri
     * @return bool
     */
    public function connect(Request $request, $redirectUri)
    {
        if (!$this->resourceOwner->handles($request)) {
            return false;
        }

        $accessToken = $this->resourceOwner->getAccessToken($request, $redirectUri);
        $this->saveToken($accessToken);

        return true;
    }

    /**
     * set oauth token to redis storage
     * @param array $token
     */
    protected function saveToken(array $token)
    {
        $token['expires_in'] = $this->getExpireTime($token);
        $this->redis->setex('token', self::TOKEN_LIFETIME, serialize($token));
        $this->token = $token;
    }

    /**
     * fetch oauth token from redis storage
     */
    protected function fetchToken()
    {
        $tokenStr = $this->redis->get('token');
        if (empty($tokenStr)) {
            $this->token = null;

            return;
        }

        $this->token = unserialize($tokenStr);
    }

    protected function refreshToken()
    {
        $token = $this->resourceOwner->refreshAccessToken($this->token['refresh_token']);
        $this->saveToken($token);
    }

    /**
     * @return string
     * @throws \Symfony\Component\Security\Core\Exception\TokenNotFoundException
     */
    public function getAccessToken()
    {
        if (empty($this->token)) {
            $this->fetchToken();
        }

        if (empty($this->token['access_token'])) {
            throw new TokenNotFoundException();
        }

        if ($this->isTokenExpired()) {
            $this->refreshToken();
        }

        return $this->token['access_token'];
    }

    /**
     * @return bool
     */
    protected function isTokenExpired()
    {
        if (time() < $this->token['expires_in']) {
            return false;
        }

        return true;
    }

    /**
     * @param array $token
     * @return int
     */
    protected function getExpireTime(array $token)
    {
        if (empty($token['expires_in'])) {
            return time();
        }

        return time() + $token['expires_in'];
    }

    /**
     * @return int
     */
    public function getTokenLastRefreshTime()
    {
        $ttl = $this->redis->ttl('token');

        return $ttl > 0 ? (self::TOKEN_LIFETIME - $ttl) : 0;
    }
} 