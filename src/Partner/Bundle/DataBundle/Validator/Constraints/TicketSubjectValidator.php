<?php
/**
 * Created by PhpStorm.
 * User: shatun
 * Date: 10/31/13
 * Time: 3:11 PM
 */

namespace Partner\Bundle\DataBundle\Validator\Constraints;


use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use Partner\Bundle\DataBundle\Entity\Ticket;
use Symfony\Bridge\Doctrine\ManagerRegistry;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class TicketSubjectValidator extends ConstraintValidator
{
    protected $doctrine;

    public function __construct(ManagerRegistry $registry)
    {
        $this->doctrine = $registry;
    }

    /**
     * @param Ticket $entity
     * @param \Partner\Bundle\DataBundle\Validator\Constraints\TicketSubject|\Symfony\Component\Validator\Constraint $constraint
     */
    public function validate($entity, Constraint $constraint)
    {
        if (!$entity instanceof Ticket) {
            return;
        }

        $site = $entity->getSite();

        if (!$site) {
            return;
        }

        $subject = $entity->getSubject();

        if (!$subject->isModerateRequest()) {
            return;
        }

        //site already has current ticket
        if ($site->getTickets()->count() <= 1) {
            return;
        }

        /* @var $em EntityManager */
        $em = $this->doctrine->getManager();
        $qb = $em->createQueryBuilder();

        $qb->select(array('t'))
            ->from(get_class($entity), 't')
            ->andWhere('t.site = ?1', 't.status != ?2')
            ->innerJoin('t.subject', 'sub', 'WITH', 'sub.moderateRequest = ?3')
            ->setParameters(array(1 => $site, 2 => Ticket::CLOSED, 3 => true));

        if ($entity->getId()) {
            $qb->andWhere('t != :ticket')
                ->setParameter('ticket', $entity);
        }

        $query = $qb->getQuery();
        $result = $query->getResult();

        if (empty($result)) {
            return;
        }

        $this->context->addViolation($constraint->message);
    }
}