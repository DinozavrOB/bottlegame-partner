<?php
/**
 * Created by PhpStorm.
 * User: shatun
 * Date: 10/31/13
 * Time: 3:11 PM
 */

namespace Partner\Bundle\DataBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class TicketSubject extends Constraint
{
    protected $service = 'partner.validator.ticket_subject';

    public $message = 'запрос на модерацию для данного сайта уже существует';

    public function validatedBy()
    {
        return $this->service;
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
} 