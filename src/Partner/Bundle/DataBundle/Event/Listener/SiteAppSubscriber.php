<?php

namespace Partner\Bundle\DataBundle\Event\Listener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\UnitOfWork;
use Partner\Bundle\BottlegameApiBundle\Service\BottlegameApi;
use Partner\Bundle\DataBundle\Entity\Setting;
use Partner\Bundle\DataBundle\Entity\Site;
use Partner\Bundle\DataBundle\Entity\SiteApp;

class SiteAppSubscriber implements EventSubscriber
{
    /**
     * @var UnitOfWork
     */
    protected $uow;

    function __construct(BottlegameApi $api)
    {
        $this->api = $api;
    }

    public function getSubscribedEvents()
    {
        return array(
            'prePersist',
            'prePersist',
            'onFlush'
        );
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        /* @var $entity SiteApp */
        $entity = $args->getEntity();

        if (!$entity instanceof SiteApp) {
            return false;
        }

        $app = $entity->getApp();

        if (!$app) {
            return false;
        }

        $userAppSettings = array();
        $appSettings = $app->getSettings();
        /* @var $setting Setting */
        foreach ($appSettings as $setting) {
            $slug = $setting->getSlug();
            $value = $setting->getDefaultValue();
            $userAppSettings[$slug] = $value;
        }

        $entity->setSettings($userAppSettings);
    }

    public function onFlush(OnFlushEventArgs $args)
    {
        $this->uow = $args->getEntityManager()->getUnitOfWork();

        foreach ($this->uow->getScheduledEntityUpdates() AS $entity) {
            if ($entity instanceof SiteApp) {
                $this->saveSettingsToApi($entity);
            }
        }
    }

    protected function saveSettingsToApi(SiteApp $siteApp)
    {
        $changeSet = $this->uow->getEntityChangeSet($siteApp);
        if (!isset($changeSet['settings'])) {
            return;
        }

        if ($siteApp->getSite()->getStatus() !== Site::STATUS_ACTIVE) {
            return;
        }

        $result = $this->api->updateSite($siteApp->getSite());

        if (!$result) {
            $error = $this->api->getLastError();
            throw new \Exception(
                'не удалось сохранить запись через api. попробуйте позже',
                null,
                new \Exception($error['message'], $error['code'])
            );
        }
    }
}

?>