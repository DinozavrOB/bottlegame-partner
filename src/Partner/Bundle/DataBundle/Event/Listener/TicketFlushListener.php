<?php

namespace Partner\Bundle\DataBundle\Event\Listener;

use Partner\Bundle\DataBundle\Entity\Ticket;
use Partner\Bundle\UserBundle\Mailer\Mailer;
use Doctrine\ORM\Event\OnFlushEventArgs;

class TicketFlushListener
{
    protected $mailer;

    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityInsertions() AS $entity) {
            if ($entity instanceof Ticket) {
                $this->sendNotification($entity);
            }
        }

        foreach ($uow->getScheduledEntityUpdates() AS $entity) {
            if ($entity instanceof Ticket) {
                $this->sendNotification($entity);
            }
        }
    }

    protected function sendNotification(Ticket $ticket)
    {
        $this->container->get('partner.mailer')->sendAdminNotification($ticket);
    }

}