<?php

namespace Partner\Bundle\DataBundle\Event\Listener;


use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\UnitOfWork;
use Partner\Bundle\BottlegameApiBundle\Service\BottlegameApi;
use Partner\Bundle\DataBundle\Entity\Site;
use Partner\Bundle\DataBundle\Entity\Ticket;
use Partner\Bundle\DataBundle\Entity\TicketHistory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\SecurityContext;

class SiteSubscriber implements EventSubscriber
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var UnitOfWork
     */
    protected $uow;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return array
     */
    public function getSubscribedEvents()
    {
        return array(
            'onFlush'
        );
    }

    public function onFlush(OnFlushEventArgs $args)
    {
        $this->em = $args->getEntityManager();
        $this->uow = $this->em->getUnitOfWork();
        $securityContext = $this->container->get('security.context');
        $hasRoleAdmin = $securityContext->getToken() ? $securityContext->isGranted('ROLE_ADMIN') : false;

        foreach ($this->uow->getScheduledEntityInsertions() AS $entity) {
            if ($entity instanceof Site) {
                if ($entity->getStatus() === Site::STATUS_MODERATE) {
                    $this->createTicket($entity);
                }

                if ($hasRoleAdmin && $entity->getStatus() === Site::STATUS_ACTIVE) {
                    $this->sendSiteToApi($entity);
                }
            }
        }

        foreach ($this->uow->getScheduledEntityUpdates() AS $entity) {
            if ($entity instanceof Site) {
                $changeSet = $this->uow->getEntityChangeSet($entity);
                if (!isset($changeSet['status'])) {
                    continue;
                }

                $statusChangeSet = $changeSet['status'];
                if ($statusChangeSet[0] === Site::STATUS_NEW && $statusChangeSet[1] === Site::STATUS_MODERATE) {
                    $this->createTicket($entity, true);
                } else {
                    if (!$hasRoleAdmin) {
                        throw new AccessDeniedException();
                    }
                    $this->createOrChangeTicket($entity);
                }

                if ($entity->getStatus() === Site::STATUS_ACTIVE) {
                    $this->sendSiteToApi($entity);
                }
            }
        }

        foreach ($this->uow->getScheduledEntityDeletions() AS $entity) {
            if ($entity instanceof Site) {
                $this->deleteSiteFormApi($entity);
            }
        }
    }

    protected function createTicket(Site $site, $recompute = false)
    {
        $ticket = new Ticket();
        $subject = $this->getModerateRequestSubject();
        $ticket->setSubject($subject)
            ->setDescription($subject->getDefaultDescription())
            ->setAuthor($site->getUser())
            ->setSite($site)
            ->setStatus(Ticket::OPEN);

        /* @var $validator \Symfony\Component\Validator\Validator */
        $validator = $this->container->get('validator');
        $errors = $validator->validate($ticket);

        if (count($errors)) {
            throw new BadRequestHttpException('ticket validation error');
        }

        $this->em->persist($ticket);

        if ($recompute) {
            $this->recomputeChangeSet($ticket);
        }
    }

    /**
     * @param Site $site
     */
    protected function createOrChangeTicket(Site $site)
    {
        $qb = $this->em->createQueryBuilder();

        $qb->select('t')
            ->from('Partner\Bundle\DataBundle\Entity\Ticket', 't')
            ->innerJoin('t.subject', 'sub', 'WITH', 'sub.moderateRequest = ?1')
            ->where('t.site = ?2')
            ->setParameters(array(1 => true, 2 => $site))
            ->setMaxResults(1);

        /* @var $ticket Ticket */
        $ticket = $qb->getQuery()->getSingleResult();

        if (!$ticket) {
            $this->createTicket($site);

            return;
        }

        $history = new TicketHistory();
        /* @var $securityContext SecurityContext */
        $securityContext = $this->container->get('security.context');
        $user = $securityContext->getToken()->getUser();
        $history->setAuthor($user)
            ->setMessage('Статус сайта изменен на ' . $site->getStatus());

        $this->em->persist($history);

        $ticket->addHistory($history);

        $this->recomputeChangeSet($history);
    }

    /**
     * @return \Partner\Bundle\DataBundle\Entity\TicketSubject
     */
    protected function getModerateRequestSubject()
    {
        $repo = $this->em->getRepository('PartnerDataBundle:TicketSubject');
        $subject = $repo->findOneBy(array('moderateRequest' => true));

        return $subject;
    }

    protected function recomputeChangeSet($entity, $recompute = false)
    {
        $classMetadata = $this->em->getClassMetadata(get_class($entity));
        if ($recompute) {
            $this->uow->recomputeSingleEntityChangeSet($classMetadata, $entity);

            return;
        }
        $this->uow->computeChangeSet($classMetadata, $entity);
    }

    protected function sendSiteToApi(Site $site)
    {
        /* @var $api BottlegameApi */
        $api = $this->container->get('bottlegame_api.api');
        if (!$site->isPersistInApi()) {
            $result = $api->createSite($site);
            $site->setPersistInApi(true);
            $this->recomputeChangeSet($site, true);
        } else {
            $result = $api->updateSite($site);
        }

        if (!$result) {
            $error = $api->getLastError();
            throw new \Exception(
                'не удалось сохранить запись через api. попробуйте позже',
                null,
                new \Exception($error['message'], $error['code'])
            );
        }
    }

    protected function deleteSiteFormApi(Site $site)
    {
        if (!$site->isPersistInApi()) {
            return false;
        }

        /* @var $api BottlegameApi */
        $api = $this->container->get('bottlegame_api.api');
        $result = $api->deleteSite($site);

        if (!$result) {
            $error = $api->getLastError();
            throw new \Exception(
                'не удалось удалить запись через api. попробуйте позже',
                null,
                new \Exception($error['message'], $error['code'])
            );
        }

        return $result;
    }
}