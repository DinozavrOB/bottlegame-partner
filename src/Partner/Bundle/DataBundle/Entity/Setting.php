<?php

namespace Partner\Bundle\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Partner\Bundle\DataBundle\Entity\Repositories\SettingRepository")
 * @ORM\Table(name="setting")
 *
 */
class Setting
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *
     * @ORM\Column(type="string", length=128)
     */
    protected $name;

    /**
     * @Gedmo\Slug(fields={"name"}, updatable=true, separator="_")
     * @ORM\Column(type="string", length=128, unique=true)
     */
    protected $slug;

    /**
     *
     * @ORM\Column(type="text")
     */
    protected $description;

    /**
     *
     * @ORM\Column(type="string", columnDefinition="ENUM('boolean', 'string', 'array')")
     */
    protected $type;

    /**
     *
     * @ORM\Column(type="string", length=256)
     */
    protected $defaultValue;

    /**
     *
     * @ORM\OneToMany(targetEntity="SettingValue", mappedBy="setting", cascade={"persist"})
     */
    protected $availableValues;

    /**
     *
     * @ORM\Column(type="string", length=64, name="system_name", unique=true)
     */
    protected $systemName;

    /**
     *
     * @ORM\Column(type="boolean", name="is_active")
     */
    protected $active;

    /**
     *
     * @ORM\Column(type="boolean", name="is_visible")
     */
    protected $visible;

    /**
     *
     * @ORM\Column(type="boolean", name="is_editable")
     */
    protected $editable;

    /**
     *
     * @ORM\ManyToOne(targetEntity="SettingGroup", inversedBy="settings")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    protected $group;

    /**
     *
     * @ORM\ManyToMany(targetEntity="Application", mappedBy="settings")
     */
    protected $apps;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     *
     */
    protected $updatedAt;

    public function __toString()
    {
        return $this->getName() ? : '-';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Setting
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Setting
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set comment
     *
     * @param string $description
     * @return Setting
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set type
     *
     * @return Setting
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set value
     *
     * @param array $value
     * @return Setting
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return array
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Setting
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     * @return Setting
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set editable
     *
     * @param boolean $editable
     * @return Setting
     */
    public function setEditable($editable)
    {
        $this->editable = $editable;

        return $this;
    }

    /**
     * Get editable
     *
     * @return boolean
     */
    public function getEditable()
    {
        return $this->editable;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Setting
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Setting
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set defaultValue
     *
     * @param string $defaultValue
     * @return Setting
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    /**
     * Get defaultValue
     *
     * @return string
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * Set systemName
     *
     * @param string $systemName
     * @return Setting
     */
    public function setSystemName($systemName)
    {
        $this->systemName = $systemName;

        return $this;
    }

    /**
     * Get systemName
     *
     * @return string
     */
    public function getSystemName()
    {
        return $this->systemName;
    }

    /**
     * Set group
     *
     * @param \Partner\Bundle\DataBundle\Entity\SettingGroup $group
     * @return Setting
     */
    public function setGroup(\Partner\Bundle\DataBundle\Entity\SettingGroup $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \Partner\Bundle\DataBundle\Entity\SettingGroup
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->availableValues = new \Doctrine\Common\Collections\ArrayCollection();
        $this->apps = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add availableValues
     *
     * @param \Partner\Bundle\DataBundle\Entity\settingValue $availableValue
     * @return Setting
     */
    public function addAvailableValue(\Partner\Bundle\DataBundle\Entity\settingValue $availableValue)
    {
        $availableValue->setSetting($this);
        $this->availableValues[] = $availableValue;

        return $this;
    }

    /**
     * Remove availableValues
     *
     * @param \Partner\Bundle\DataBundle\Entity\SettingValue $availableValue
     */
    public function removeAvailableValue(\Partner\Bundle\DataBundle\Entity\SettingValue $availableValue)
    {
        $availableValue->setSetting(null);
        $this->availableValues->removeElement($availableValue);
    }

    /**
     * Get availableValues
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAvailableValues()
    {
        return $this->availableValues;
    }

    public function getAvailableValuesChoices()
    {
        $choices = array();
        /* @var $settingValue SettingValue */
        foreach ($this->getAvailableValues() as $settingValue) {
            $choices[$settingValue->getValue()] = $settingValue->getLabel();
        }

        return $choices;
    }

    /**
     * Add apps
     *
     * @param \Partner\Bundle\DataBundle\Entity\Application $apps
     * @return Setting
     */
    public function addApp(\Partner\Bundle\DataBundle\Entity\Application $apps)
    {
        $this->apps[] = $apps;

        return $this;
    }

    /**
     * Remove apps
     *
     * @param \Partner\Bundle\DataBundle\Entity\Application $apps
     */
    public function removeApp(\Partner\Bundle\DataBundle\Entity\Application $apps)
    {
        $this->apps->removeElement($apps);
    }

    /**
     * Get apps
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApps()
    {
        return $this->apps;
    }
}