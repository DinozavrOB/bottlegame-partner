<?php

namespace Partner\Bundle\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="setting_group")
 */
class SettingGroup
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=32)
     * @var type
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=128)
     * @Gedmo\Slug(fields={"name"}, updatable=true, separator="_")
     */
    protected $slug;

    /**
     *
     * @ORM\Column(type="integer")
     */
    protected $sort;

    /**
     * @ORM\OneToMany(targetEntity="Setting", mappedBy="group")
     * @var type
     */
    protected $settings;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->settings = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->name ? : '-';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return SettingGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return SettingGroup
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set sort
     *
     * @param integer $sort
     * @return SettingGroup
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Add settings
     *
     * @param \Partner\Bundle\DataBundle\Entity\Setting $settings
     * @return SettingGroup
     */
    public function addSetting(\Partner\Bundle\DataBundle\Entity\Setting $settings)
    {
        $this->settings[] = $settings;

        return $this;
    }

    /**
     * Remove settings
     *
     * @param \Partner\Bundle\DataBundle\Entity\Setting $settings
     */
    public function removeSetting(\Partner\Bundle\DataBundle\Entity\Setting $settings)
    {
        $this->settings->removeElement($settings);
    }

    /**
     * Get settings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSettings()
    {
        return $this->settings;
    }
}