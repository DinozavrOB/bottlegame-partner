<?php
/**
 * Created by PhpStorm.
 * User: shatun
 * Date: 10/31/13
 * Time: 12:02 PM
 */

namespace Partner\Bundle\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="ticket_subject")
 */
class TicketSubject
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Gedmo\Slug(fields={"title"}, updatable=true, separator="_")
     * @ORM\Column(type="string", length=64)
     */
    protected $slug;

    /**
     * @ORM\Column(type="string", length=32)
     */
    protected $title;

    /**
     * @ORM\Column(type="boolean", name="is_active")
     */
    protected $active = true;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     *
     */
    protected $updatedAt;

    /**
     * @ORM\Column(type="boolean", name="is_moderate_request")
     */
    protected $moderateRequest = false;

    /**
     * @ORM\Column(type="boolean", name="is_selectable")
     */
    protected $selectable = true;

    /**
     * @ORM\Column(type="string", name="default_description", length = 1024, nullable=true)
     */
    protected $defaultDescription;

    public function __toString()
    {
        return $this->title ? : 'new';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return TicketSubject
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set subject
     *
     * @param string $title
     * @return TicketSubject
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return TicketSubject
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return TicketSubject
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return TicketSubject
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set moderateRequest
     *
     * @param boolean $moderateRequest
     * @return TicketSubject
     */
    public function setModerateRequest($moderateRequest)
    {
        $this->moderateRequest = $moderateRequest;

        return $this;
    }

    /**
     * Get moderateRequest
     *
     * @return boolean
     */
    public function isModerateRequest()
    {
        return $this->moderateRequest;
    }

    /**
     * Set selectable
     *
     * @param boolean $selectable
     * @return TicketSubject
     */
    public function setSelectable($selectable)
    {
        $this->selectable = $selectable;

        return $this;
    }

    /**
     * Get selectable
     *
     * @return boolean
     */
    public function isSelectable()
    {
        return $this->selectable;
    }

    /**
     * @param mixed $defaultDescription
     */
    public function setDefaultDescription($defaultDescription)
    {
        $this->defaultDescription = $defaultDescription;
    }

    /**
     * @return mixed
     */
    public function getDefaultDescription()
    {
        return $this->defaultDescription;
    }

    public function isDefaultDescriptionRequired()
    {
        if ($this->isModerateRequest() && !mb_strlen(trim($this->getDefaultDescription()))) {
            return true;
        }

        return false;
    }
}