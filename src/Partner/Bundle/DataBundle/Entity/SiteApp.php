<?php

namespace Partner\Bundle\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Partner\Bundle\DataBundle\Entity\Application;
use Gedmo\Mapping\Annotation as Gedmo;
use Partner\Bundle\DataBundle\Entity\Site;

/**
 * @ORM\Entity
 * @ORM\Table(name="site_app")
 */
class SiteApp
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Partner\Bundle\DataBundle\Entity\Site", inversedBy="apps")
     * @ORM\JoinColumn(name="site_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $site;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Partner\Bundle\DataBundle\Entity\Application", inversedBy="sites")
     * @ORM\JoinColumn(name="app_id", referencedColumnName="id")
     */
    protected $app;

    /**
     *
     * @ORM\Column(type="array")
     */
    protected $settings;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     *
     */
    protected $updatedAt;

    public function __isset($name)
    {
        return isset($this->settings[$name]);
    }

    public function __call($name, $arguments)
    {
        if (substr($name, 0, 3) === 'get') {
            $property = substr($name, 3);

            return $this->$property;
        }

        if (substr($name, 0, 3) === 'set') {
            $property = substr($name, 3);
            $this->$property = $arguments[0];

            return $this;
        }
    }

    public function __get($name)
    {
        return array_key_exists($name, $this->settings) ? $this->settings[$name] : null;
    }

    public function __set($name, $value)
    {
        $this->settings[$name] = $value;
    }

    public function __toString()
    {
        $app = $this->getApp();

        return ($app && $app->getName()) ? $app->getName() : '-';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set settings
     *
     * @param array $settings
     * @return SiteApp
     */
    public function setSettings($settings)
    {
        $this->settings = $settings;

        return $this;
    }

    /**
     * Get settings
     *
     * @return array
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * Set site
     *
     * @param Site $site
     * @return SiteApp
     */
    public function setSite(Site $site = null)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return Site
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set app
     *
     * @param Application $app
     * @return SiteApp
     */
    public function setApp(Application $app = null)
    {
        $this->app = $app;

        return $this;
    }

    /**
     * Get app
     *
     * @return \Partner\Bundle\DataBundle\Entity\Application
     */
    public function getApp()
    {
        return $this->app;
    }

    /**
     * Set createdAt
     *
     * @param DateTime $createdAt
     * @return Site
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param DateTime $updatedAt
     * @return Site
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}