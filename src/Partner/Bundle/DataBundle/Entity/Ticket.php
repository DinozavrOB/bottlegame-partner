<?php

namespace Partner\Bundle\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Partner\Bundle\DataBundle\Entity\TicketHistory;
use Partner\Bundle\DataBundle\Entity\Site;
use Partner\Bundle\DataBundle\Entity\TicketSubject;
use Partner\Bundle\UserBundle\Entity\User;

/**
 * @ORM\Entity
 * @ORM\Table(name="ticket")
 */
class Ticket
{
    const OPEN = 'open';
    const CLOSED = 'closed';
    const REOPEN = 'reopen';


    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Partner\Bundle\UserBundle\Entity\User", inversedBy="tickets")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     * @var User
     */
    protected $author;

    /**
     * @ORM\ManyToOne(targetEntity="Partner\Bundle\DataBundle\Entity\TicketSubject")
     * @ORM\JoinColumn(name="subject_id", referencedColumnName="id")
     */
    protected $subject;

    /**
     * @todo: подумать нужно ли это. может сюда какой-то шаблон брать для письма и добавлять историю
     * @ORM\Column(type="text")
     */
    protected $description;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('open', 'closed', 'reopen')")
     */
    protected $status = 'open';

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     *
     */
    protected $updatedAt;

    /**
     *
     * @ORM\OneToMany(targetEntity="Partner\Bundle\DataBundle\Entity\TicketHistory", mappedBy="ticket")
     */
    protected $history;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Partner\Bundle\DataBundle\Entity\Site", inversedBy="tickets")
     * @ORM\JoinColumn(name="site_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $site;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->history = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->id . '';
    }

    public function isNew()
    {
        return $this->id == null;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param TicketSubject $subject
     * @return Ticket
     */
    public function setSubject(TicketSubject $subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get title
     *
     * @return TicketSubject
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Ticket
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set status
     *
     * @return Ticket
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Ticket
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Ticket
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add history
     *
     * @param TicketHistory $history
     * @return Ticket
     */
    public function addHistory(TicketHistory $history)
    {
        $this->history[] = $history;
        $history->setTicket($this);

        return $this;
    }

    /**
     * Remove history
     *
     * @param TicketHistory $history
     */
    public function removeHistory(TicketHistory $history)
    {
        $this->history->removeElement($history);
    }

    /**
     * Get history
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHistory()
    {
        return $this->history;
    }

    /**
     * Set site
     *
     * @param Site $site
     * @return Ticket
     */
    public function setSite(Site $site = null)
    {
        $this->site = $site;
        $site->addTicket($this);

        return $this;
    }

    /**
     * Get site
     *
     * @return Site
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set author
     *
     * @param User $author
     * @return Ticket
     */
    public function setAuthor(User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \Partner\Bundle\UserBundle\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    public static function getStatusChoices()
    {
        $choices = array(self::OPEN, self::CLOSED, self::REOPEN);

        return array_combine($choices, $choices);
    }
}