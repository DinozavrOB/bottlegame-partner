<?php

namespace Partner\Bundle\DataBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="application")
 */
class Application
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *
     * @ORM\Column(type="string", length=64)
     */
    protected $name;

    /**
     * @Gedmo\Slug(fields={"name"}, updatable=true, separator="_")
     * @ORM\Column(type="string", length=64, unique=true)
     */
    protected $slug;

    /**
     *
     * @ORM\Column(type="string", length=64, name="system_name")
     */
    protected $systemName;

    /**
     *
     * @ORM\Column(type="text")
     */
    protected $description;

    /**
     *
     * @ORM\ManyToMany(targetEntity="Partner\Bundle\DataBundle\Entity\Setting", inversedBy="apps")
     * @ORM\JoinTable(name="app_settings",
     *      joinColumns={@ORM\JoinColumn(name="app_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="setting_id", referencedColumnName="id")}
     *      )
     */
    protected $settings;

    /**
     *
     * @ORM\OneToMany(targetEntity="Partner\Bundle\DataBundle\Entity\SiteApp", mappedBy="app")
     */
    protected $sites;

    /**
     *
     * @ORM\Column(type="boolean", name="is_active")
     */
    protected $active;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     *
     */
    protected $updatedAt;

    public function __toString()
    {
        return $this->getName() ? : '-';
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->settings = new ArrayCollection();
        $this->sites = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Application
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Application
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Application
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Application
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Application
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Application
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add settings
     *
     * @param \Partner\Bundle\DataBundle\Entity\Setting $settings
     * @return Application
     */
    public function addSetting(\Partner\Bundle\DataBundle\Entity\Setting $settings)
    {
        $this->settings[] = $settings;

        return $this;
    }

    /**
     * Remove settings
     *
     * @param \Partner\Bundle\DataBundle\Entity\Setting $settings
     */
    public function removeSetting(\Partner\Bundle\DataBundle\Entity\Setting $settings)
    {
        $this->settings->removeElement($settings);
    }

    /**
     * Get settings
     *
     * @return \Doctrine\ORM\PersistentCollection
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * Add sites
     *
     * @param \Partner\Bundle\DataBundle\Entity\SiteApp $sites
     * @return Application
     */
    public function addSite(\Partner\Bundle\DataBundle\Entity\SiteApp $sites)
    {
        $this->sites[] = $sites;

        return $this;
    }

    /**
     * Remove sites
     *
     * @param \Partner\Bundle\DataBundle\Entity\SiteApp $sites
     */
    public function removeSite(\Partner\Bundle\DataBundle\Entity\SiteApp $sites)
    {
        $this->sites->removeElement($sites);
    }

    /**
     * Get sites
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSites()
    {
        return $this->sites;
    }

    /**
     * @param mixed $systemName
     */
    public function setSystemName($systemName)
    {
        $this->systemName = $systemName;
    }

    /**
     * @return mixed
     */
    public function getSystemName()
    {
        return $this->systemName;
    }

}