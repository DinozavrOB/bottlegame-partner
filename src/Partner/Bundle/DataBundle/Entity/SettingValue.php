<?php

namespace Partner\Bundle\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="setting_value")
 */
class SettingValue
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *
     * @ORM\Column(type="string", length=64)
     */
    protected $label;

    /**
     *
     * @ORM\Column(type="string", length=32)
     */
    protected $value;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Setting", inversedBy="availableValues")
     * @ORM\JoinColumn(name="setting_id", referencedColumnName="id")
     */
    protected $setting;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return SettingValue
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set setting
     *
     * @param \Partner\Bundle\DataBundle\Entity\Setting $setting
     * @return SettingValue
     */
    public function setSetting(\Partner\Bundle\DataBundle\Entity\Setting $setting = null)
    {
        $this->setting = $setting;

        return $this;
    }

    /**
     * Get setting
     *
     * @return \Partner\Bundle\DataBundle\Entity\Setting
     */
    public function getSetting()
    {
        return $this->setting;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return SettingValue
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
}