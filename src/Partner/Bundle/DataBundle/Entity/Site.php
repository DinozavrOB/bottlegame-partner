<?php

namespace Partner\Bundle\DataBundle\Entity;

use Partner\Bundle\DataBundle\Entity\SiteApp;
use Partner\Bundle\DataBundle\Entity\Ticket;
use Partner\Bundle\UserBundle\Entity\User;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="site")
 */
class Site
{
    const STATUS_NEW = 'new';
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';
    const STATUS_REJECTED = 'rejected';
    const STATUS_MODERATE = 'moderate';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *
     * @ORM\Column(type="string", length=128)
     */
    protected $name;

    /**
     * @Gedmo\Slug(fields={"name"}, updatable=true, separator="_")
     * @ORM\Column(type="string", length=128, unique=true)
     */
    protected $slug;

    /**
     *
     * @ORM\Column(type="integer")
     */
    protected $visits;

    /**
     *
     * @ORM\Column(type="string", length=128)
     */
    protected $link;

    /**
     * @var PersistentCollection
     * @ORM\OneToMany(targetEntity="Partner\Bundle\DataBundle\Entity\SiteApp", mappedBy="site")
     */
    protected $apps;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Partner\Bundle\UserBundle\Entity\User", inversedBy="sites")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     *
     */
    protected $updatedAt;

    /**
     *
     * @ORM\Column(type="string", columnDefinition="ENUM('new','active', 'moderate', 'rejected', 'inactive')")
     */
    protected $status = self::STATUS_NEW;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="Partner\Bundle\DataBundle\Entity\Ticket", mappedBy="site")
     */
    protected $tickets;

    /**
     * @var
     * @ORM\Column(type="integer")
     */
    protected $percent = 50;

    /**
     * @var int
     * @ORM\Column(type="float")
     */
    protected $ratio = 1;

    /**
     * @var
     * @ORM\Column(type="boolean", name="persist_in_api")
     */
    protected $persistInApi = false;

    public function __toString()
    {
        return $this->getName() ? : '-';
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->apps = new ArrayCollection();
        $this->tickets = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Site
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Site
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set visits
     *
     * @param integer $visits
     * @return Site
     */
    public function setVisits($visits)
    {
        $this->visits = $visits;

        return $this;
    }

    /**
     * Get visits
     *
     * @return integer
     */
    public function getVisits()
    {
        return $this->visits;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return Site
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set createdAt
     *
     * @param DateTime $createdAt
     * @return Site
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param DateTime $updatedAt
     * @return Site
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add apps
     *
     * @param SiteApp $app
     * @return Site
     */
    public function addApp(SiteApp $app)
    {
        if (!$this->apps->contains($app)) {
            $this->apps->add($app);
            $app->setSite($this);
        }

        return $this;
    }

    /**
     * Remove apps
     *
     * @param SiteApp $app
     * @return Site
     */
    public function removeApp(SiteApp $app)
    {
        if ($this->apps->contains($app)) {
            $this->apps->removeElement($app);
            $app->setSite(null);
        }

        return $this;
    }

    /**
     * Get apps
     *
     * @return PersistentCollection <Application>
     */
    public function getApps()
    {
        return $this->apps;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return Site
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function isNew()
    {
        return $this->id == null;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    public static function getStatusChoices()
    {
        $choices = array(
            self::STATUS_NEW,
            self::STATUS_MODERATE,
            self::STATUS_ACTIVE,
            self::STATUS_REJECTED,
            self::STATUS_INACTIVE
        );

        return array_combine($choices, $choices);
    }

    /**
     * Add tickets
     *
     * @param Ticket $tickets
     * @return Site
     */
    public function addTicket(Ticket $tickets)
    {
        $this->tickets[] = $tickets;

        return $this;
    }

    /**
     * Remove tickets
     *
     * @param Ticket $tickets
     */
    public function removeTicket(Ticket $tickets)
    {
        $this->tickets->removeElement($tickets);
    }

    /**
     * Get tickets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTickets()
    {
        return $this->tickets;
    }

    /**
     * @param mixed $percent
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;
    }

    /**
     * @return mixed
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * @param int $ratio
     */
    public function setRatio($ratio)
    {
        $this->ratio = $ratio;
    }

    /**
     * @return int
     */
    public function getRatio()
    {
        return $this->ratio;
    }

    /**
     * @param mixed $persistInApi
     */
    public function setPersistInApi($persistInApi)
    {
        $this->persistInApi = $persistInApi;
    }

    /**
     * @return mixed
     */
    public function isPersistInApi()
    {
        return $this->persistInApi == true;
    }


}