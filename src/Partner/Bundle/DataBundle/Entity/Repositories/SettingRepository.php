<?php
/**
 * Created by PhpStorm.
 * User: shatun
 * Date: 11/19/13
 * Time: 5:33 PM
 */

namespace Partner\Bundle\DataBundle\Entity\Repositories;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query;

class SettingRepository extends EntityRepository
{
    public function fetchAll($indexBy)
    {
        return $this->createQueryBuilder('s')
            ->add('from', new Expr\From($this->getClassName(), 's', 's.' . $indexBy), false)
            ->getQuery()
            ->useResultCache(true, 60)
            ->getResult();
    }
} 