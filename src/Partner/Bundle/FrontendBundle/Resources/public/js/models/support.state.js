/**
 * Created by shatun on 11/6/13.
 */
define([
    'jquery',
    'underscore',
    'backbone'
], function ($, _, Backbone) {
    var SupportStateModel = Backbone.Model.extend({
        defaults: {
            filter: ['open', 'reopen'],
            sort: 'date',
            asc: false
        },
        isActiveStatus: function (status) {
            if (_.isEmpty(status)) {
                return false;
            }

            if (_.indexOf(this.get('filter'), status) !== -1) {
                return true;
            } else {
                return false;
            }

        }
    });

    return (new SupportStateModel());
});