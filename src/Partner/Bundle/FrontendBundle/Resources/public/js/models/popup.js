/**
 * Created by shatun on 11/7/13.
 */
define([
    'jquery-pkg',
    'underscore',
    'backbone-pkg'
], function ($, _, Backbone) {
    var PopupModel = Backbone.Model.extend({
        initialize: function (options) {
            this.id = Backbone.history.getFragment();
            this.on('change:zindex', this.onZindexChange, this);
            var self = this;
            this.get('view').$el.on('click', function () {
                if (_.isObject(self.collection)) {
                    self.collection.moveToTop(self.id);
                }
            })
        },
        defaults: {
            view: null,
            zindex: null
        },
        onZindexChange: function (model, value) {
            var self = this;
            if (!self.previous('zindex')) {
                return false;
            }

            this.get('view').$el.css('zIndex', value);
        }
    });

    return PopupModel;
});