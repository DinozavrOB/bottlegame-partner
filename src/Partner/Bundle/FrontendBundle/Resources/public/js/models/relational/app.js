define([
    'jquery-pkg',
    'underscore',
    'backbone-pkg'
], function ($, _, Backbone) {
    var AppRelationalModel = Backbone.RelationalModel.extend({
        defaults: {
            id: null,
            name: '',
            description: ''
        }
    });

    return AppRelationalModel;
});

