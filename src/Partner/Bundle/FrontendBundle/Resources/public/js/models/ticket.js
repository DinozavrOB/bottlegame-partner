/**
 * Created by shatun on 11/1/13.
 */
define(['jquery-pkg',
    'underscore',
    'backbone-pkg'
], function ($, _, Backbone) {
    var TicketModel = Backbone.Model.extend({
        url: Routing.generate('partner_frontend_ticket_new'),
        defaults: {
            id: null,
            site: null,
            author: null,
            subject: null,
            description: null,
            status: 'open',
            created_at: null,
            updated_at: null,
            history: {}
        },
        parse: function (response, options) {
            var data = response;
            data.created_at = new Date(response.created_at);
            data.updated_at = new Date(response.updated_at);

            return data;
        }
    });

    return TicketModel;
});