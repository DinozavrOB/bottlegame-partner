define([
    'jquery-pkg',
    'underscore',
    'backbone'
], function ($, _, Backbone) {
    var SiteappSettingsModel = Backbone.Model.extend({
        settings: [],
        parse: function (res) {
            this.settings = res.settings;
            return res.siteapp_settings;
        }
    });

    return SiteappSettingsModel;
});


