define([
    'jquery',
    'underscore',
    'backbone-pkg'
], function ($, _, Backbone) {
    var SiteModel = Backbone.Model.extend({
        url: Routing.generate('partner_frontend_site_new'),
        defaults: {
            visits: null,
            link: null,
            name: null,
            status: 'new'
        }
    });

    return SiteModel;
});