define([
    'jquery',
    'underscore',
    'backbone-pkg',
    'collections/siteapp',
    'models/relational/siteapp',
    'models/relational/ticket',
    'collections/ticket'
], function ($, _, Backbone, SiteappCollection, SiteappRelationalModel, TicketRelationModel, TicketCollection) {
    var SiteRelationalModel = Backbone.RelationalModel.extend({
        relations: [
            {
                type: Backbone.HasMany,
                key: 'apps',
                relatedModel: SiteappRelationalModel,
                collectionType: SiteappCollection,
                reverseRelation: {
                    key: 'site',
                    includeInJSON: true
                }
            },
            {
                type: Backbone.HasMany,
                key: 'tickets',
                relatedModel: TicketRelationModel,
                collectionType: TicketCollection,
                reverseRelation: {
                    key: 'site',
                    includeInJSON: true
                }
            }
        ],
        defaults: {
            apps: {},
            visits: null,
            link: null,
            name: null,
            status: null,
            tickets: {}
        }
    });

    return SiteRelationalModel;
});
