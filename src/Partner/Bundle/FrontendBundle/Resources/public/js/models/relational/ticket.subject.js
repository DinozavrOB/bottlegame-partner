/**
 * Created by shatun on 11/1/13.
 */
define([
    'jquery-pkg',
    'underscore',
    'backbone-pkg',
    'models/relational/ticket',
    'collections/ticket'
], function ($, _, Backbone, TicketRelationalModel, TicketCollection) {
    var TicketSubjectRealtionalModel = Backbone.RelationalModel.extend({
        relations: [
            {
                type: Backbone.HasMany,
                key: 'tickets',
                relatedModel: TicketRelationalModel,
                collectionType: TicketCollection,
                reverseRelation: {
                    key: 'subject',
                    includeInJSON: true
                }
            }
        ],
        defaults: {
            slug: null,
            title: null,
            selectable: false,
            moderate_request: false,
            tickets: null
        }
    });

    return TicketSubjectRealtionalModel;
});