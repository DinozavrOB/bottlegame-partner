define([
    'jquery-pkg',
    'underscore',
    'backbone-pkg'
], function ($, _, Backbone) {
    var UserModel = Backbone.Model.extend({
        url: Routing.generate('partner_frontend_my_get'),
        initialize: function () {
            this.on('sync', this.clearPasswords, this);
            this.fetch();
        },
        defaults: {
            username: null,
            email: null,
            firstname: null,
            lastname: null,
            cur_password: null,
            plainPassword: null
        },
        clearPasswords: function () {
            this.set({'cur_password': null, 'plainPassword': null}, {silent: true});
        }
    });

    return UserModel;
})

