/**
 * Created by shatun on 11/1/13.
 */
define([
    'jquery-pkg',
    'underscore',
    'backbone-pkg',
    'models/relational/ticket.history',
    'collections/ticket.history'
], function ($, _, Backbone, TicketHistoryRelationalModel, TicketHistoryCollection) {
    var TicketRelationModel = Backbone.RelationalModel.extend({
        relations: [
            {
                type: Backbone.HasMany,
                key: 'history',
                relatedModel: TicketHistoryRelationalModel,
                collectionType: TicketHistoryCollection,
                reverseRelation: {
                    key: 'ticket',
                    includeInJSON: 'id'
                }
            }
        ],
        defaults: {
            id: null,
            site: null,
            author: null,
            subject: null,
            description: null,
            status: null,
            created_at: null,
            updated_at: null,
            history: {}
        },
        workflow: {
            'open': ['closed'],
            'reopen': ['closed'],
            'closed': ['reopen']
        },
        url: function () {
            return Routing.generate('partner_frontend_ticket_get', {id: this.id})
        },
        changeStatus: function (status, options) {
            if (!_.has(this.workflow, status)) {
                return;
            }

            var curStatus = this.get('status'), self = this;
            if (status === curStatus) {
                return;
            }

            if (!this.isValidTransition(curStatus, status)) {
                window.notifications.trigger('error', 'изменение статуса невозможно');
                return;
            }

            var options = _.extend({
                patch: true,
                wait: true,
                status_changed: true,
                error: function (model, xhr, options) {
                    window.notifications.trigger('error', 'изменение статуса невозможно');
                }
            }, options);

            this.save({status: status}, options);

            return this;
        },
        isValidTransition: function (prev, next) {
            var allowTransition = this.workflow[prev];
            if (!_.isEmpty(allowTransition) && _.indexOf(allowTransition, next) !== -1) {
                return true;
            }

            return false;
        },
        remove: function (opt) {
            if (this.get('status') === 'closed') {
                this.destroy({
                    wait: true,
                    success: function (model, response, options) {
                        window.notifications.trigger('success', 'тикет успешно удален');
                        if (_.isFunction(opt.success)) {
                            opt.success(model, response, options);
                        }
                    },
                    error: function (model, xhr, options) {
                        window.notifications.trigger('error', 'не удалось удалить тикет');
                        if (_.isFunction(opt.error)) {
                            opt.error(model, xhr, options);
                        }
                    }
                });
            } else {
                window.notifications.trigger('error', 'чтобы удалить тикет его нужно закрыть');
            }

            return false;
        }
    });

    return TicketRelationModel;
});