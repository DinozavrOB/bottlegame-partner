/**
 * Created by shatun on 11/21/13.
 */
define([
    'jquery-pkg',
    'underscore',
    'backbone'
], function ($, _, Backbone) {
    var SiteapApiUrlModel = Backbone.Model.extend({
        url: function () {
            return Routing.generate('partner_frontend_siteapp_geturl', {
                site_id: this.site_id,
                id: this.app_id
            });
        },
        site_id: null,
        app_id: null,
        initialize: function (options) {
            this.site_id = options.site_id;
            this.app_id = options.app_id;
        },
        defaults: {
            apiurl: null
        }
    });

    return SiteapApiUrlModel;
});