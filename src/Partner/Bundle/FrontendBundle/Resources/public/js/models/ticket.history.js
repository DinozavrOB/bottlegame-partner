/**
 * Created by shatun on 11/9/13.
 */

define([
    'jquery-pkg',
    'underscore',
    'backbone-pkg'
], function ($, _, Backbone) {
    var TicketHistoryModel = Backbone.Model.extend({
        url: function () {
            return Routing.generate('partner_frontend_ticket_history_new', {ticket_id: this.get('ticket')});
        },
        defaults: {
            ticket: null,
            author: null,
            message: null,
            created_at: null
        }
    });

    return TicketHistoryModel;
});
