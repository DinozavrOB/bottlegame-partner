/**
 * Created by shatun on 11/1/13.
 */
define([
    'jquery-pkg',
    'underscore',
    'backbone-pkg',
    'models/relational/ticket'
], function ($, _, Backbone, TicketRelationalModel) {
    var TicketHistoryRelationalModel = Backbone.RelationalModel.extend({
//        relations: [{
//            type: Backbone.HasOne,
//            key: 'ticket',
//            relatedModel: TicketRelationalModel,
//            reverseRelation: {
//                key: 'history',
//                includeInJSON: 'id'
//            }
//        }],
//        defaults: {
//            ticket: null,
//            author: null,
//            message: null,
//            created_at: null
//        }
    });

    return TicketHistoryRelationalModel;
});