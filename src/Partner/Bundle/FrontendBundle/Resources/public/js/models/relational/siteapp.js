define([
    'jquery-pkg',
    'underscore',
    'backbone-pkg',
    'models/relational/app'
], function ($, _, Backbone, AppRelationalModel) {
    var SiteappRelationalModel = Backbone.RelationalModel.extend({
        relations: [
            {
                type: Backbone.HasOne,
                key: 'app',
                relatedModel: AppRelationalModel,
                reverseRelation: {
                    key: 'siteapp',
                    includeInJSON: 'id'
                }
            }
        ],
        defaults: {
            id: '', //integer
            app: {}, //app model
            settings: [], //array
            updated_at: null//date
        }
    });

    return SiteappRelationalModel;
});


