define([
    'jquery',
    'underscore',
    'backbone-pkg',
//    'router', // Request router.js
    'views/app'
], function ($, _, Backbone, AppView) {
    var initialize = function () {
        // Pass in our Router module and call it's initialize function
        Backbone.emulateHTTP = false;
        Backbone.emulateJSON = false;
        window.notifications = {};
        _.extend(window.notifications, Backbone.Events);
        $('body').append(new Notifier({
            model: window.notifications,
            wait: 3000
        }).render().el
        );

        var app_view = AppView;
    }

    return {
        initialize: initialize
    };
});