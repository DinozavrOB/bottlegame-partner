requirejs.config({
//    baseUrl: 'js',
    paths: {
        jquery: 'lib/jquery/jquery-1.10.2.min',
        jqueryui: 'lib/jquery/jquery-ui-1.10.3.custom.min',
        backbone: 'lib/backbone/backbone',
        'backbone-pkg': 'lib/backbone/backbone-pkg',
        underscore: 'lib/underscore-min',
        'jquery-pkg': 'lib/jquery/jquery-pkg',
        'backbone-forms': 'lib/backbone/plugins/forms/backbone-forms'
    },
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'backbone-pkg': {
            deps: ['backbone']
        },
        'jqueryui': {
            deps: ['jquery']
        },
        'backbone-forms': {
            deps: ['backbone']
        }
    }
});

require(['lib/domReady'], function (domReady) {
    domReady(function () {
        require(['app'], function (App) {
            // The "app" dependency is passed in as "App"
            App.initialize();
        });
    });
});
