/**
 * Created by shatun on 11/7/13.
 */
define([
], function () {
    var DraggableMixin = {
        handle: '.drag-handle',
        cursor: "move",
        drag: function () {
            if (_.isEmpty(this.handle)) {
                return false;
            }

            var $el = this.$(this.handle);
            if (_.isEmpty($el)) {
                return false;
            }

            var self = this;
            $el.css('cursor', this.cursor)
                .on("mousedown", function (e) {
                    $(this).addClass('active-handle');
                    var $drag = self.$el.addClass('draggable');

                    var z_idx = $drag.css('z-index'),
                        drg_h = $drag.outerHeight(),
                        drg_w = $drag.outerWidth(),
                        pos_y = $drag.offset().top + drg_h - e.pageY,
                        pos_x = $drag.offset().left + drg_w - e.pageX;
                    $drag.css('z-index', 1000).on("mousemove", function (e) {
                        if (self.$el.hasClass('draggable')) {
                            self.$el.offset({
                                top: e.pageY + pos_y - drg_h,
                                left: e.pageX + pos_x - drg_w
                            }).on("mouseup", function () {
                                self.$el
                                    .removeClass('draggable')
                                    .css('z-index', z_idx)
                                    .off("mousemove");
                            });
                        }
                    });
                    e.preventDefault(); // disable selection
                })
                .on("mouseup", function () {
                    $(this).removeClass('active-handle');
                    self.$el
                        .removeClass('draggable')
                        .off("mousemove");
                });
        }
    }

    return DraggableMixin;
});