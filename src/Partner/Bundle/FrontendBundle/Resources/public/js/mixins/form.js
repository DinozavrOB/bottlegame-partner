define([
    'jquery',
    'underscore',
    'backbone'
], function ($, _, Backbone) {
    var FormMixin = {
        prepareFormTabs: function () {
            var $navTab = this.form.$('div.nav-tab').empty();
            this.form.$('a.nav-tab-item').appendTo($navTab);
            this.form.$('div.data-tab:first').addClass('first');
            this.form.$('.styler').styler();
            this.switchTab(0);
        },
        prepareSingleForm: function () {
            this.form.$('div.nav-tab').remove();
            this.form.$('a.nav-tab-item').remove();
            this.form.$('div.data-tab')
                .show()
                .find('.styler')
                .styler();
        },
        switchTab: function (index) {
            var i = parseInt(index)
            this.form.$('a.nav-tab-item').removeClass('active').eq(i).addClass('active');
            this.form.$('div.data-tab')
                .hide()
                .eq(i)
                .show();

            this.refreshStyler();
        },
        onNavTabClick: function (e) {
            var index = this.form.$('a.nav-tab-item').index($(e.target));
            this.switchTab(index);

            return false;
        },
        refreshStyler: function () {
            this.form.$('.styler').trigger('refresh');
        },
        formSubmit: function () {
            var errors = this.form.commit(),
                self = this;

            if (!errors) {
                this.model.save({}, {
                    success: function (model, response, options) {
                        window.notifications.trigger('success', 'Успешно сохранено');
                        self.$el.bPopup().close();
                    },
                    error: function (model, xhr, options) {
                        var message = 'Произошла ошибка';
                        if (xhr.status === 400 && xhr.responseJSON.message) {
                            message = xhr.responseJSON.message;
                            self.setFormErrors(xhr.responseJSON.errors);
                        }
                        window.notifications.trigger('error', message);
                    }
                });
            } else {
                window.notifications.trigger('error', 'Ошибка валидации');
            }
        },
        setFormErrors: function (errors) {
            if (_.isEmpty(errors.children)) {
                return;
            }

            var fields = this.form.fields;

            _.each(errors.children, function (el, key) {
                if (_.isEmpty(el) && !_.isArray(el.errors)) {
                    return;
                }

                if (!fields[key] instanceof Backbone.Form.Field) {
                    return;
                }

                if (_.isEmpty(el.errors)) {
                    return;
                }

                var msg = el.errors.join(' ');
                fields[key].setError(msg);
            });

            this.form.setFieldsetError(errors.children);
        }
    };

    return FormMixin;
});

