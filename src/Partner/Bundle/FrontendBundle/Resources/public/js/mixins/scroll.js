/**
 * Created by shatun on 11/8/13.
 */
define([
    'jquery-pkg',
    'underscore',
    'backbone-pkg',
    'lib/jquery/jquery.mCustomScrollbar.concat.min'
], function ($, _, Backbone) {
    var ScrollMixin = {
        scroll: function () {
            this.$el.mCustomScrollbar({
                theme: "dark",
                scrollButtons: {
                    enable: false
                }
            });
        },
        scrollUpdate: function () {
            this.$el.mCustomScrollbar("update");
        }
    }

    return ScrollMixin;
});