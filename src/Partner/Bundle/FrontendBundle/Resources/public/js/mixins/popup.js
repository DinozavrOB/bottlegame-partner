define([
    'jquery',
    'underscore',
    'backbone',
    'models/popup',
    'collections/popup'
], function ($, _, Backbone, PopupModel, PopupCollection) {
    var PopupMixin = {
        popupModel: null,
        popupCollection: PopupCollection,
        createPopup: function () {
            if (_.isObject(this.popupModel)) {
                return;
            }
            var self = this;

            this.popupModel = new PopupModel({
                view: this
            });
            this.popupCollection.push(this.popupModel);
            this.$el.bPopup({
                modal: this.isModal,
                escClose: true,
                follow: [false, false],
                closeClass: 'close-reveal-modal',
                zIndex: this.popupModel.get('zindex'),
                onClose: function () {
                    self.popupCollection.remove(self.popupModel);
                    self.popupModel = null;
                }
            });
        }
    };

    return PopupMixin;
});
