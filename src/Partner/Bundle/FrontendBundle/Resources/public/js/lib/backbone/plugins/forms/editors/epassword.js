define(['jquery', 'underscore', 'backbone', 'backbone-forms'], function ($, _, Backbone) {
    ;
    (function (Form) {

        Form.editors.ePassword = Form.editors.Password.extend({
            $inputField: null,
            $typeSwitcher: null,
            initialize: function (options) {
                Form.editors.Password.prototype.initialize.call(this, options);
                this.$inputField = this.$el;
                this.$typeSwitcher = $('<a/>').addClass('change-password icon eye')
                var $div = $('<div/>').append(this.$el).append(this.$typeSwitcher),
                    self = this;

                this.setElement($div);

                this.$typeSwitcher.on('click', function (e) {
                    e.stopPropagation();
                    var currentType = self.$inputField.attr('type'),
                        newType = (currentType === 'text') ? 'password' : 'text';

                    self.$inputField.attr('type', newType);
                    self.$typeSwitcher.toggleClass('active');
                });
            },
            getValue: function () {
                return this.$inputField.val();
            },

            setValue: function (value) {
                this.$inputField.val(value);
            },

            focus: function () {
                if (this.hasFocus) return;

                this.$inputField.focus();
            }
        });
    })(Backbone.Form);
});