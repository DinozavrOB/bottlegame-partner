/**
 * Include this template file after backbone-forms.amd.js to override the default templates
 *
 * 'data-*' attributes control where elements are placed
 */
define(['jquery', 'underscore', 'backbone', 'backbone-forms'], function ($, _, Backbone) {
    var Form = Backbone.Form;


    /**
     * Bootstrap templates for Backbone Forms
     */
    Form.template = _.template('\
    <form class="modal-form">\
        <div class="tabs" data-fieldsets>\
            <div class="nav-tab"></div>\
        </div>\
    </form>\
  ');


    Form.Fieldset.template = _.template('\
    <div class="data-tab">\
        <a href="#" class="nav-tab-item">\
            <% if (legend) { %>\
                <%= legend %>\
            <% } %>\
        </a>\
        <table class="table-form">\
            <tbody data-fields></tbody>\
        </table>\
    </div>\
  ');


    Form.Field.template = _.template('\
    <tr control-group field-<%= key %>>\
        <td class="label"><label for="<%= editorId %>"><%= title %></label></td>\
        <td align="right" data-editor>\
        </td>\
        <td><span style="display:none;" class="validate" data-error></span></td>\
    </tr>\
  ');


    Form.NestedField.template = _.template('\
    <div class="field-<%= key %>">\
      <div title="<%= title %>" class="input-xlarge">\
        <span data-editor></span>\
        <div class="help-inline" data-error></div>\
      </div>\
      <div class="help-block"><%= help %></div>\
    </div>\
  ');


    if (Form.editors.List) {

        Form.editors.List.template = _.template('\
      <div class="bbf-list">\
        <ul class="unstyled clearfix" data-items></ul>\
        <button type="button" class="btn bbf-add" data-action="add">Add</button>\
      </div>\
    ');


        Form.editors.List.Item.template = _.template('\
      <li class="clearfix">\
        <div class="pull-left" data-editor></div>\
        <button type="button" class="btn bbf-del" data-action="remove">&times;</button>\
      </li>\
    ');


        Form.editors.List.Object.template = Form.editors.List.NestedModel.template = _.template('\
      <div class="bbf-list-modal"><%= summary %></div>\
    ');

    }


});