define([
    'backbone',
    'lib/backbone/plugins/backbone-relational',
    'lib/backbone/plugins/backbone.collectionView.min',
    'lib/backbone/plugins/backbone.notifications',
    'backbone-forms',
    'lib/backbone/plugins/forms/templates/partner'
], function (Backbone) {
    return Backbone;
})