define([
    'jquery',
    'underscore',
    'backbone-pkg',
    'lib/backbone/plugins/forms/partner-field',
    'lib/backbone/plugins/forms/templates/partner',
    'lib/backbone/plugins/forms/editors/epassword'
], function ($, _, Backbone, PartnerFormField) {
    var PartnerForm = Backbone.Form.extend({
        Field: PartnerFormField,
        $currentTab: null,
        validate: function (options) {
            var errors = Backbone.Form.prototype.validate.apply(this, options);

            this.setFieldsetError(errors);

            return errors;
        },
        setFieldsetError: function (errors) {
            var self = this,
                $fieldset = this.$('[data-fields]'),
                $navTab = this.$('.nav-tab-item');

            $navTab.removeClass('error');

            _.each(errors, function (obj, name) {
                if (_.isEmpty(obj)) {
                    return;
                }

                var field = self.fields[name], index;

                if (!field instanceof self.Field) {
                    return;
                }

                index = $fieldset.index(field.$el.closest('[data-fields]'));

                $navTab.eq(index).addClass('error');
            });
        }


    });

    return PartnerForm;
});