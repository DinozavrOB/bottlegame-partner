Twig.setFunction('path', function (path, data) {
    return Routing.generate(path, data);
});

Twig.setFunction('url', function (path, data) {
    return Routing.generate(path, data, true);
});
