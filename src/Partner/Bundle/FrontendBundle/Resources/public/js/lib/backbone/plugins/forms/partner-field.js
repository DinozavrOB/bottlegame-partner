define([
    'jquery',
    'underscore',
    'backbone',
    'backbone-forms'
], function ($, _, Backbone) {
    var PartnerFormField = Backbone.Form.Field.extend({
        setError: function (msg) {
            if (this.editor.hasNestedForm)
                return;

            this.$('[data-error]')
                .addClass(this.errorClassName)
                .prop('title', msg)
                .show();
        },
        clearError: function () {
            this.$('[data-error]')
                .removeClass(this.errorClassName)
                .removeAttr('title')
                .hide();
        }
    });

    return PartnerFormField;
})

