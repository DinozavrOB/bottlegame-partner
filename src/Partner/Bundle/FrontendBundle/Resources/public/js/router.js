define([
    'jquery-pkg',
    'underscore',
    'backbone',
    'views/app',
    'collections/popup'
], function ($, _, Backbone, AppView, PopupCollection) {
    var AppRouter = Backbone.Router.extend({
        initialize: function (options) {
            this.user = options.user;
            this.sites = options.sites;
            this.popupCollection = PopupCollection;
            this.ticketSubjects = options.ticketSubjects;
        },
        routes: {
            '': 'home',
            'my': 'profileShow',
            stat: 'statShow',
            payment: 'paymentShow',
            'support': 'supportShow',
            'sites/new': 'siteAdd',
            'sites/:id': 'siteShow',
            'sites/:id/edit': 'siteEdit',
            'sites/:site_id/apps/new': 'siteappAdd',
            'sites/:site_id/apps/:id': 'siteappShow',
            'sites/:site_id/apps/:id/geturl': 'showAppUrl',
            'sites/:site_id/apps/:id/edit': 'siteappEdit',
            'sites/:site_id/tickets': 'siteTicketsShow',
            'sites/:site_id/tickets/new': 'siteTicketsAdd',
            'tickets/new': 'ticketAdd',
            'tickets/:ticket_id': 'ticketShow',
            // Default
            '*actions': 'defaultAction'
        },
        titles: {

        },
        history: {},
        home: function () {
            //@todo: закрыть все окна
//            console.log('home');
        },
        siteAdd: function () {
            var self = this;
            require([
                'models/site',
                'views/site.form'
            ], function (SiteModel, SiteFormView) {
                var siteModel = new SiteModel(),
                    siteFormView = new SiteFormView({
                        model: siteModel,
                        collection: self.sites
                    });

                siteFormView.render();
            });
        },
        siteShow: function (id) {
            console.log('site show', id);
        },
        siteEdit: function (id) {
            console.log('site edit ', id);
            var model = this.sites.get(id);

            if (!model) {
                return false;
            }

            require([
                'views/site.form'
            ], function (SiteFormView) {
                var siteFormView = new SiteFormView({
                    model: model
                });

                siteFormView.render();
            });
        },
        siteappAdd: function () {
            console.log('siteapp add');
        },
        siteappShow: function (id) {
            console.log('siteapp show', id);
        },
        siteappEdit: function (site_id, id) {
            var self = this;

            require([
                'models/siteapp.settings',
                'views/siteapp.settings'
            ], function (SiteappSettingsModel, SiteappSettingsView) {
                var siteappModel = self.sites.get(site_id).get('apps').get(id),
                    siteappSettingsModel = new SiteappSettingsModel();

                siteappSettingsModel.url = Routing.generate('partner_frontend_siteapp_settings_get', {id: id});
                siteappSettingsModel.fetch({
                    success: function (model, res, opt) {
                        var siteappSettingsView = new SiteappSettingsView({
                            model: model,
                            siteapp: siteappModel
                        });

                        siteappSettingsView.render();
                    }
                });

            });
        },
        showAppUrl: function (site_id, app_id) {
            require([
                'views/siteapp.apiurl',
                'models/siteapp.apiurl'
            ], function (SiteappApiUrlView, SiteapApiUrlModel) {
                var siteappApiUrlModel = new SiteapApiUrlModel({
                    site_id: site_id,
                    app_id: app_id
                }), siteappApiUrlView = new SiteappApiUrlView({
                    model: siteappApiUrlModel
                });

                siteappApiUrlModel.fetch({
                    success: function (model, res, opt) {
                        siteappApiUrlView.render();
                    },
                    error: function (model, xhr, opt) {
                        window.notifications.trigger('error', 'не удалось получить ссылку');
                    }
                });
            })
        },
        profileShow: function () {
            console.log('user profile show');
            var self = this;
            require([
                'views/user.profile'
            ], function (UserProfileView) {
                var userProfileView = new UserProfileView({model: self.user});
                userProfileView.render();
            });

        },
        supportShow: function () {
            console.log('support_show');
            require([
                'views/support'
            ], function (SupportView) {
                var supportView = new SupportView();
                supportView.render();
            });
        },
        ticketAdd: function () {
            console.log('ticket add');
            var self = this;
            require([
                'views/ticket.form',
                'models/ticket'
            ], function (TicketFormView, TicketModel) {
                var ticketFormView = new TicketFormView({
                    sites: self.sites,
                    ticketSubjects: self.ticketSubjects,
                    model: new TicketModel()
                });
                ticketFormView.render();
            });
        },
        ticketShow: function (id) {
            require([
                'models/relational/ticket',
                'views/ticket'
            ], function (TicketRelationModel, TicketView) {
                var ticket = TicketRelationModel.find({id: id}),
                    ticketView;

                if (_.isEmpty(ticket)) {
                    ticket = TicketRelationModel.findOrCreate({id: id});
                    ticket.fetch({
                        success: function (model, response, options) {
                            ticketView = new TicketView({model: model});
                            ticketView.render();
                        },
                        error: function (model, xhr, options) {
                            window.notifications.trigger('error', 'тикет с номером ' + id + ' не найден');
                        }
                    });

                    return;
                }

                ticketView = new TicketView({model: ticket});
                ticketView.render();
            });
        },
        defaultAction: function (actions) {
            console.log('No route:', actions);
        },
        goToHome: function () {
            console.log('go to home');
            this.navigate('');

            return this;
        },
        bindLinks: function () {
            var self = this;
            $(document).on('click', 'a:not([data-bypass])', function (evt) {
                var href = $(this).attr("href"),
                    protocol = this.protocol + "//";

                console.log('navigate to:', href);
                if (href && href.slice(0, protocol.length) !== protocol && href.indexOf("javascript:") !== 0 && href !== "#") {
                    evt.preventDefault();
                    var fragment = Backbone.history.getFragment(href);
                    if (self.popupCollection.hasPopup(fragment)) {
                        self.popupCollection.moveToTop(fragment);
                    } else {
                        Backbone.history.navigate(fragment, true);
                    }
                }
            });
        }
    });

    return AppRouter;
});