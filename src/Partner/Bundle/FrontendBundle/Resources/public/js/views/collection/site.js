define([
    'jquery',
    'underscore',
    'backbone-pkg',
    'collections/site',
    'views/site'
], function ($, _, Backbone, SiteCollection, SiteView) {
    var SiteCollectionView = Backbone.CollectionView.extend({
        el: $('#site_collection'),
        initialize: function () {
            this.collection.on('reset', this.render, this);
            this.collection.on('add', this.addSite, this);
        },
        render: function () {
            this.$el.empty();
            this.collection.each(this.addSite, this);

            return this;
        },
        addSite: function (model) {
            var v = new SiteView({model: model});
            this.$el.append(v.render().el);
        }
    });

    return SiteCollectionView;
});

