/**
 * Created by shatun on 11/8/13.
 */
define([
    'jquery',
    'underscore',
    'backbone-pkg',
    'views/ticket.history'
], function ($, _, Backbone, TicketHistoryView) {
    var TicketHistoryCollectionView = Backbone.View.extend({
        initialize: function (options) {
            this.collection.on('add', this.addHistory, this);
        },
        events: {

        },
        render: function () {
            this.$el.empty();

            this.collection.each(this.addHistory, this);

            return this;
        },
        addHistory: function (model) {
            var ticketHistoryView = new TicketHistoryView({
                model: model
            });

            this.$el.append(ticketHistoryView.render().el);
        }
    });

    return TicketHistoryCollectionView;
});