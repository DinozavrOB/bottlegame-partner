define([
    'jquery-pkg',
    'underscore',
    'backbone-pkg',
    'mixins/form',
    'mixins/popup',
    'mixins/draggable',
    'lib/backbone/plugins/forms/templates/partner'
], function ($, _, Backbone, FormMixin, PopupMixin, DraggableMixin) {
    var SiteappSettingsView = Backbone.View.extend({
        tagName: 'div',
        className: 'reveal-modal modalBlue w450',
        events: {
            'click #submit_button': 'formSubmit',
            'click .nav-tab-item': 'onNavTabClick'
        },
        siteapp: {},
        site: {},
        isModal: false,
        initialize: function (options) {
            this.siteapp = options.siteapp;
        },
        render: function () {
            var html = Twig.render(siteapp_settings, {
                    site: this.siteapp.get('site').toJSON(),
                    siteapp: this.siteapp.toJSON()
                }),
                self = this;

            this.form = new Backbone.Form({
                model: this.model,
                schema: this.getSchema(),
                fieldsets: this.getFieldsets()
            }).render();

            this.prepareFormTabs();
            this.$el
                .html(html)
                .appendTo($('body'))
                .find('#siteapp_form')
                .append(this.form.el);

            this.createPopup();
            this.drag();
            this.refreshStyler();

            return this;
        },
        getSchema: function () {
            var schema = {};
            _.each(this.model.settings, function (obj) {
                _.each(obj.settings, function (setting) {
                    schema[setting.slug] = _.extend({
                            editorClass: 'styler',
                            title: setting.name,
                            help: setting.description
                        },
                        this.resolveType(setting)
                    );
                }, this);
            }, this);

            return schema;
        },
        resolveType: function (setting) {
            switch (setting.type) {
                case 'boolean':
                    return {type: 'Select', options: [
                        {val: 1, label: 'Yes'},
                        {val: 0, label: 'No'}
                    ]};
                case 'string':
                    return {type: 'Text'};
                    break;
                case 'array':
                    var options = [];
                    _.each(setting.available_values, function (v) {
                        options.push({
                            val: v.value,
                            label: v.label
                        });
                    });
                    return {type: 'Select', options: options};
                    break;
                default:
                    return {};
            }
        },
        getFieldsets: function () {
            var fieldsets = [];
            _.each(this.model.settings, function (obj) {
                var fields = [];
                _.each(obj.settings, function (setting) {
                    fields.push(setting.slug);
                });
                var fieldset = {
                    legend: obj.group.name,
                    fields: fields
                };
                fieldsets.push(fieldset);
            });

            return fieldsets;
        }
    });

    _.extend(SiteappSettingsView.prototype, FormMixin, PopupMixin, DraggableMixin);

    return SiteappSettingsView;
});
