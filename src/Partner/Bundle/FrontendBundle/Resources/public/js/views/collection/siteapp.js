define([
    'jquery',
    'underscore',
    'backbone-pkg',
    'collections/siteapp',
    'views/siteapp'
], function ($, _, Backbone, SiteappCollection, SiteappView) {
    var SiteappCollectionView = Backbone.CollectionView.extend({
        modelView: SiteappView,
        collection: SiteappCollection,
        initialize: function () {
            this.listenTo(this.collection, 'reset', this.render);
        },
        render: function () {
            var $el = this.$el;

            $el.find(':only-child').not(':first').remove();
            this.collection.each(this.addSiteapp, this);

            return this;
        },
        addSiteapp: function (model) {
            var siteappView = new SiteappView({model: model});
            this.$el.append(siteappView.render().el);
        }
    });

    return SiteappCollectionView;
});