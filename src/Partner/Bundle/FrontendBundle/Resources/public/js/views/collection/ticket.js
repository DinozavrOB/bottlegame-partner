/**
 * Created by shatun on 11/1/13.
 */
define([
    'jquery-pkg',
    'underscore',
    'backbone-pkg',
    'views/ticket.item',
    'collections/ticket',
    'models/support.state'
], function ($, _, Backbone, TicketItemView, TicketCollection, SupportStateModel) {
    var TicketCollectionView = Backbone.View.extend({
        currentTickets: {},
        ready: false,
        initialize: function () {
            this.collection = new TicketCollection();
            this.collection.on('add', this.ticketAdd, this);
            this.supportState = SupportStateModel;
            this.supportState.on('change:sort change:asc', this.onSupportSortChange, this);
            this.supportState.on('change:filter', this.render, this);
            this.collection.on('sort', this.render, this);
            this.collection.on('change', this.onChange, this);
            this.setComparator(this.supportState.get('sort'));
        },
        itemViews: {},
        onChange: function (model, options) {
            if (options.status_changed) {
                this.onModelStatusChanged(model);
                return;
            }
        },
        onModelStatusChanged: function (model) {
            if (!this.supportState.isActiveStatus(model.get('status'))) {
                return;
            }

            this.collection.sort();
        },
        collectionFetch: function () {
            console.log('collection fetch');
            var self = this;
            this.collection.fetch({
                reset: true,
                error: function (collection, response, options) {
                    window.notifications.trigger('error', 'не удалось загрузить коллекцию тикетов');
                },
                success: function (collection, response, options) {
                    self.currentTickets = collection.models;
                    self.ready = true;
                }
            });

            return this;
        },
        render: function () {
            this.$el.empty();
            this.removeItemViews();

            this.collection.each(this.addTicket, this);

            return this;
        },
        removeItemViews: function () {
            _.each(this.itemViews, function (view) {
                view.remove()
            });
            this.itemViews = {};
        },
        addTicket: function (model) {
            if (!this.supportState.isActiveStatus(model.get('status'))) {
                return;
            }

            var v = new TicketItemView({model: model});
            this.$el.append(v.render().el);
            this.itemViews[v.cid] = v;
            v.once('remove', this.onRemoveItemView, this);
        },
        onRemoveItemView: function (options) {
            if (!_.has(this.itemViews, options.cid)) {
                return;
            }

            this.updateTicketList(options.status);
            delete this.itemViews[options.cid];
        },
        onSupportSortChange: function (model, value, options) {
            var sortType = model.get('sort');

            this.setComparator(sortType)
                .collection.sort();

            return this;
        },
        setComparator: function (sortType) {
            var sortFn;

            switch (sortType) {
                case 'date':
                    sortFn = this._sortByDate;
                    break;
                case 'subject':
                    sortFn = this._sortBySubject;
                    break;
                case 'status':
                    sortFn = this._sortByStatus;
                    break;
                case 'site':
                    sortFn = this._sortBySite;
                    break;
            }

            this.collection.comparator = _.bind(sortFn, this);

            return this;
        },
        _filterOpenTickets: function (model) {
            return model.get('status') !== 'closed';
        },
        _filterClosedTickets: function (model) {
            return model.get('status') === 'closed';
        },
        _sortByDate: function (m1, m2) {
            var v1 = m1.get('updated_at'),
                v2 = m2.get('updated_at');

            return this._compareValues(v1, v2, this.supportState.get('asc'));
        },
        _sortBySubject: function (m1, m2) {
            var v1 = m1.get('subject').get('title').toLowerCase(),
                v2 = m2.get('subject').get('title').toLowerCase();

            return this._compareValues(v1, v2, this.supportState.get('asc'));
        },
        _sortByStatus: function (m1, m2) {
            var v1 = m1.get('status'),
                v2 = m2.get('status');

            return this._compareValues(v1, v2, this.supportState.get('asc'));
        },
        _sortBySite: function (m1, m2) {
            //wrong sort. fix it!
            return null;
        },
        _compareValues: function (v1, v2, asc) {
            if (v1 === v2) {
                return 0;
            }

            var res = (v1 > v2) ? 1 : -1;
            return asc ? res : res * (-1);
        }
    });

    return (new TicketCollectionView());
});