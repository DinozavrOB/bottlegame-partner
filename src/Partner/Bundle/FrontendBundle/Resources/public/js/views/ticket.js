/**
 * Created by shatun on 11/1/13.
 */
define([
    'jquery-pkg',
    'underscore',
    'backbone-pkg',
    'views/collection/ticket.history',
    'models/ticket.history',
    'models/relational/ticket.history',
    'mixins/popup',
    'mixins/draggable'
], function ($, _, Backbone, TicketHistoryCollectionView, TicketHistoryModel, TicketHistoryRelationalModel, PopupMixin, DraggableMixin) {
    var TicketView = Backbone.View.extend({
        className: 'reveal-modal modalPurple w680',
        isModal: false,
        events: {
            'click button.ticket-send': 'sendMessage',
            'keypress textarea#message-txt': 'onTextareaKeypress',
            'click div.actions .js_delete': 'removeTicket',
            'click div.actions .js_close': 'closeTicket',
            'click div.actions .js_reopen': 'reopenTicket',
            'click button.js_reopen': 'reopenTicket',
            'click button.js_close': 'closeTicket'
        },
        initialize: function (options) {
            this.listenTo(this.model, 'change:status', this.render);
        },
        alreadyAppend: false,
        render: function () {
            console.log('render ticket');
            var html, site,
                obj = {
                    ticket: this.model.toJSON(),
                    subject: this.model.get('subject').toJSON()
                };

            site = this.model.get('site');
            if (!_.isEmpty(site)) {
                _.extend(obj, {
                    site: site.toJSON()
                });
            }

            html = Twig.render(ticket, obj);
            this.$el.html(html);

            this.renderHistory(this.$('#history_list'));

            //@todo: move to popup mixin
            if (!this.alreadyAppend) {
                this.$el.appendTo($('body'));
                this.alreadyAppend = true;
            }

            this.createPopup();
            this.drag();

            return this;
        },
        renderHistory: function (el) {
            var historyView = new TicketHistoryCollectionView({
                collection: this.model.get('history'),
                el: el
            });
            historyView.render();
        },
        sendMessage: function () {
            var message = this.$('#message-txt'),
                message_txt = message.val(),
                ticketHistoryModel,
                self = this;

            if (this.model.get('status') === 'closed') {
                window.notifications.trigger('error', 'нельзя добавлять собщения в закрытый тикет');
                return false;
            }

            if (_.isEmpty(message_txt)) {
                return false;
            }

            ticketHistoryModel = new TicketHistoryModel({
                message: message_txt,
                ticket: this.model.id
            });

            ticketHistoryModel.save({}, {
                success: function (model, response, options) {
                    var history = TicketHistoryRelationalModel.findOrCreate(response),
                        collection = self.model.get('history');

                    collection.add(history);
                    message.val('');
                },
                error: function (model, xhr, options) {
                    window.notifications.trigger('error', 'не удалось добавить сообщение');
                }
            });

            return false;
        },
        onTextareaKeypress: function (e) {
            if (e.keyCode === 13 && !e.shiftKey) {
                this.sendMessage();
                return false;
            }
        },
        removeTicket: function () {
            var successFn = _.bind(this.remove, this);

            this.model.remove({
                success: successFn
            });

            return false;
        },
        closeTicket: function () {
            var successFn = _.bind(function () {
                window.notifications.trigger('success', 'тикет успешно перемещен в архив');
            }, this);

            this.model.changeStatus('closed', {success: successFn});

            return false;
        },
        reopenTicket: function () {
            var successFn = _.bind(function () {
                window.notifications.trigger('success', 'тикет успешно открыт заново)');
            }, this);

            this.model.changeStatus('reopen', {success: successFn});

            return false;
        }
    });

    _.extend(TicketView.prototype, PopupMixin, DraggableMixin);

    return TicketView;
});