define([
    'jquery',
    'underscore',
    'backbone-pkg',
    'views/collection/siteapp'
], function ($, _, Backbone, SiteappCollectionView) {
    var SiteView = Backbone.View.extend({
        tagName: 'li',
        className: 'projects-wrap',
        events: {
            "click a.js_to_moderate": 'sendToModerate',
            "click a.js_reload": 'reloadModel'
        },
        initialize: function () {
            this.siteappCollectionView = new SiteappCollectionView({
                collection: this.model.get('apps')
            });
            this.listenTo(this.model, 'change', this.render);
        },
        render: function () {
            var html = Twig.render(site_list_block, {site: this.model.toJSON()});
            this.$el.html(html);
            this.siteappCollectionView
                .setElement(this.$('ul.siteapps'))
                .render();

            return this;
        },
        sendToModerate: function () {
            var status = this.model.get('status');
            if (status !== 'new' && !_.isEmpty(status)) {
                return false;
            }

            this.model.save({status: 'moderate'}, {
                patch: true,
                wait: true,
                error: function (model, xhr, options) {
                    window.notifications.trigger('error', 'Произошла ошибка');
                },
                success: function (model, response, options) {
                    window.notifications.trigger('success', 'Статус изменен');
                }
            });

            return false;
        },
        reloadModel: function () {
            this.model.fetch({
                success: function (model, response, options) {
                    window.notifications.trigger('success', 'данные обновлены');
                },
                error: function (model, xhr, options) {
                    window.notifications.trigger('error', 'произошла ошибка');
                }
            });
            return false;
        }
    });

    return SiteView;
});


