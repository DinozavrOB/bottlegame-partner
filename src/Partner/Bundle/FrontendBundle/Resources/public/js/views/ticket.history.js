/**
 * Created by shatun on 11/1/13.
 */
define(['jquery-pkg',
    'underscore',
    'backbone-pkg'
], function ($, _, Backbone) {
    var TicketHistoryView = Backbone.View.extend({
        tagName: 'li',
        className: 'ticket-message-item clear',
        model: null,
        events: {

        },
        render: function () {
            var obj = {
                history: this.model.toJSON()
            };
            var html = Twig.render(history_item, obj);

            this.$el.html(html);

            return this;
        }
    });

    return TicketHistoryView;
});