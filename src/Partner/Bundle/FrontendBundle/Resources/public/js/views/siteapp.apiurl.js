/**
 * Created by shatun on 11/20/13.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'mixins/popup'
], function ($, _, Backbone, PopupMixin) {
    var SiteappApiUrlView = Backbone.View.extend({
        tagName: 'div',
        className: 'reveal-modal modalOrange w450',
        isModal: true,
        render: function () {
            var html = Twig.render(siteapp_apiurl, {data: this.model.toJSON()});
            this.$el.empty().html(html).appendTo($('body'));
            this.createPopup();
        }
    });

    _.extend(SiteappApiUrlView.prototype, PopupMixin);

    return SiteappApiUrlView;
});