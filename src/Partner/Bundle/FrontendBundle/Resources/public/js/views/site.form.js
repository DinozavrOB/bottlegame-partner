define([
    'jquery-pkg',
    'underscore',
    'backbone-pkg',
    'models/relational/site',
    'mixins/form',
    'mixins/popup'
], function ($, _, Backbone, SiteRelationalModel, FormMixin, PopupMixin) {
    var SiteFormView = Backbone.View.extend({
        className: 'reveal-modal modalGreen w450',
        events: {
            'click #submit_button': 'siteFormSubmit'
        },
        initialize: function () {
        },
        form: null,
        isModal: true,
        render: function () {
            var html = Twig.render(site_form, {
                    site: this.model.toJSON(),
                    isNew: this.model.isNew()
                }),
                self = this;

            this.form = new Backbone.Form({
                model: this.model,
                schema: this.getSchema()
            }).render();

            this.prepareSingleForm();

            this.$el
                .html(html)
                .appendTo($('body'));

            this.createPopup();

            this.$('#site_form').append(this.form.el);
            this.refreshStyler();

            return this;
        },
        close: function () {
            this.trigger('close');
            this.remove();
        },
        getSchema: function () {
            return {
                name: {type: 'Text', validators: ['required'], editorClass: 'field-while'},
                visits: {type: 'Text', validators: ['required'], editorClass: 'field-while'},
                link: {type: 'Text', dataType: 'url', validators: ['required', 'url'], editorClass: 'field-while'}
            }
        },
        siteFormSubmit: function () {
            var errors = this.form.commit({ validate: true }),
                self = this;

            if (!errors) {
                this.model.save(this.model.changed, {
                    wait: true,
                    patch: true,
                    success: function (model, res, options) {
                        if (options.xhr.status === 201) {
                            var siteRelationModel = SiteRelationalModel.findOrCreate(res);
                            self.collection.add(siteRelationModel);
                            model = null;
                            window.notifications.trigger('success', 'Проект успешно добавлен');
                        } else if (options.xhr.status === 204) {
                            window.notifications.trigger('success', 'Проект успешно сохранен');
                        }
                        self.$el.bPopup().close();
                    },
                    error: function () {
                        window.notifications.trigger('error', 'Произошла ошибка');
                    }
                });
            } else {
                window.notifications.trigger('error', 'Ошибка валидации');
                console.log(errors);
            }
        }
    });

    _.extend(SiteFormView.prototype, FormMixin, PopupMixin);

    return SiteFormView;
});