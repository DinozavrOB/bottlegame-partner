/**
 * Created by shatun on 11/1/13.
 */
define(['jquery-pkg',
    'underscore',
    'backbone-pkg'
], function ($, _, Backbone) {
    var TicketItemView = Backbone.View.extend({
        tagName: 'li',
        className: '',
        events: {
            'click div.actions .js_delete': 'removeTicket',
            'click div.actions .js_close': 'closeTicket',
            'click div.actions .js_reopen': 'reopenTicket'
        },
        initialize: function () {
            this.listenTo(this.model, 'change:status', this.onStatusChange)
        },
        render: function () {
            var obj = {ticket: this.model.toJSON()},
                html = Twig.render(ticket_list_row, obj);

            this.$el.html(html);

            return this;
        },
        removeTicket: function () {
            var successFn = _.bind(this.remove, this);

            this.model.remove({
                success: successFn
            });

            return false;
        },
        closeTicket: function () {
            var successFn = _.bind(function () {
                window.notifications.trigger('success', 'тикет успешно перемещен в архив');
            }, this);

            this.model.changeStatus('closed', {success: successFn});

            return false;
        },
        reopenTicket: function () {
            var successFn = _.bind(function () {
                window.notifications.trigger('success', 'тикет успешно открыт заново)');
            }, this);

            this.model.changeStatus('reopen', {success: successFn});

            return false;
        },
        onStatusChange: function (model, value, options) {
            if (!_.has(options, 'status_changed')) {
                return;
            }

            this.model.off('change:status', this.onStatusChange, this);
            this.remove();
        }
    });

    return TicketItemView
});