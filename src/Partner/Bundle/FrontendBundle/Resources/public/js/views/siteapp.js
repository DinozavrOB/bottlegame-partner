define([
    'jquery',
    'underscore',
    'backbone-pkg'
], function ($, _, Backbone) {
    var SiteappView = Backbone.View.extend({
        tagName: 'li',
        events: {

        },
        initialzie: function () {
            this.listenTo(this.model, 'change', this.render);
        },
        render: function () {
            var obj = {siteapp: this.model.toJSON(), site: this.model.get('site').toJSON()};
            var html = Twig.render(siteapp_list_row, obj);
            this.$el.html(html);

            return this;
        },
        siteappSettings: function () {

        }
    });

    return SiteappView;
});


