/**
 * Created by shatun on 11/1/13.
 */
define(['jquery-pkg',
    'underscore',
    'backbone-pkg',
    'models/relational/ticket',
    'views/collection/ticket',
    'mixins/form',
    'mixins/popup'
], function ($, _, Backbone, TicketRelationalModel, TicketCollectionView, FormMixin, PopupMixin) {
    var TicketFormView = Backbone.View.extend({
        className: 'reveal-modal modalPurple w450',
        events: {
            'click #submit_button': 'ticketFormSubmit'
        },
        initialize: function (options) {
            this.subjects = options.ticketSubjects;
            this.sites = options.sites;
            this.ticketCollectionView = TicketCollectionView;
        },
        form: null,
        isModal: true,
        render: function () {
            var html = Twig.render(ticket_form),
                self = this;

            this.form = new Backbone.Form({
                model: this.model,
                schema: this.getSchema()
            }).render();

            this.prepareSingleForm();

            this.$el
                .html(html)
                .appendTo($('body'));

            this.createPopup();

            this.$('#ticket_form').append(this.form.el);
            this.refreshStyler();

            return this;
        },
        getSchema: function () {
            var siteOptions = [
                {val: '', label: ' -- Пусто --'}
            ], subjectOptions = [];
            this.sites.each(function (site) {
                siteOptions.push({
                    label: site.get('name'),
                    val: site.id
                });
            });
            this.subjects.each(function (subject) {
                if (!subject.get('selectable')) {
                    return;
                }
                subjectOptions.push({
                    label: subject.get('title'),
                    val: subject.id
                });
            });

            return {
                site: {type: 'Select', title: 'Проект', options: siteOptions, editorClass: 'field-while styler'},
                subject: {type: 'Select', title: 'Тема', options: subjectOptions, editorClass: 'field-while styler'},
                description: {type: 'TextArea', title: 'Описание', editorClass: 'field-while'}
            }
        },
        ticketFormSubmit: function () {
            console.log('submit');
            var errors = this.form.commit({ validate: true }),
                self = this;

            if (!errors) {
                this.model.save(this.model.changed, {
                    wait: true,
                    patch: true,
                    success: function (model, res, options) {
                        var ticketRelationModel = TicketRelationalModel.findOrCreate(res, {create: true});
                        model = null;
                        window.notifications.trigger('success', 'Тикет успешно добавлен');
                        self.$el.bPopup().close();
                        if (self.ticketCollectionView.ready) {
                            self.ticketCollectionView.collection.add(ticketRelationModel);
                        } else {
                            Backbone.history.navigate('support', true);
                        }
                    },
                    error: function () {
                        window.notifications.trigger('error', 'Произошла ошибка');
                    }
                });
            } else {
                window.notifications.trigger('error', 'Ошибка валидации');
                console.log(errors);
            }

            return false;
        }
    });

    _.extend(TicketFormView.prototype, FormMixin, PopupMixin);

    return TicketFormView;
});