/**
 * Created by shatun on 11/5/13.
 */
define([
    'jquery-pkg',
    'underscore',
    'backbone-pkg',
    'models/support.state',
    'views/collection/ticket',
    'mixins/popup',
    'mixins/draggable'
], function ($, _, Backbone, SupportStateModel, TicketCollectionView, PopupMixin, DraggableMixin) {
    var SupportView = Backbone.View.extend({
        className: 'reveal-modal modalPurple w680 h503',
        ticketCollectionView: null,
        events: {
            'click a[data-sort]': 'setSortState',
            'click a[data-filter]': 'setFilterState'
        },
        $navTabs: null,
        $sortLinks: null,
        isModal: false,
        alreadyAppend: false,
        initialize: function () {
            this.model = SupportStateModel;
            this.listenTo(this.model, 'change', this.onModelChange);
            this.ticketCollectionView = TicketCollectionView;
        },
        render: function () {
            var html = Twig.render(support);

            this.$el.empty().html(html);

            if (!this.alreadyAppend) {
                this.$el.appendTo($('body'));
                this.alreadyAppend = true;
            }

            this.createPopup();
            this.drag();

            this.ticketCollectionView.setElement(this.$('ul.ticket-list'));
            this.$navTabs = this.$('div.nav-tab > a');
            this.$sortLinks = this.$('div.ticket-sort >a');

            if (!this.ticketCollectionView.ready) {
                this.ticketCollectionView
                    .collectionFetch()
                    .collection.once('reset', this.onTicketCollectionReset, this);
            } else {
                this.onModelChange();
                this.ticketCollectionView.render();
            }

            return this;
        },
        onTicketCollectionReset: function () {
            this.onModelChange();
            this.ticketCollectionView
                .render();
        },
        onModelChange: function () {
            this.setActiveFilter();
            this.setActiveSort();
        },
        setActiveFilter: function () {
            var filter = this.model.get('filter');
            this.$navTabs
                .removeClass('active')
                .filter('[data-filter="' + filter[0] + '"]')
                .addClass('active');
        },
        setActiveSort: function () {
            var sort = this.model.get('sort');
            this.$sortLinks
                .removeClass('active')
                .filter('[data-sort="' + sort + '"]')
                .addClass('active');
        },
        setSortState: function (e) {
            var sort = $(e.target).data('sort'), asc;
            if (sort === this.model.get('sort')) {
                asc = this.model.get('asc') ? false : true;
                this.model.set('asc', asc);
                return false;
            }

            this.model.set('sort', sort);

            return false;
        },
        setFilterState: function (e) {
            var targetFilter = $(e.target).data('filter'), filter, asc;
            if (_.indexOf(this.model.get('filter'), targetFilter) >= 0) {
                asc = this.model.get('asc') ? false : true;
                this.model.set('asc', asc);
                return;
            }
            filter = (targetFilter === 'closed') ? ['closed'] : ['open', 'reopen'];
            this.model.set('filter', filter);

            return false;
        }
    });

    _.extend(SupportView.prototype, PopupMixin, DraggableMixin);

    return SupportView;
});