define([
    'jquery',
    'underscore',
    'backbone-pkg',
    'mixins/form',
    'mixins/popup',
    'lib/backbone/plugins/forms/partner-form'
], function ($, _, Backbone, FormMixin, PopupMixin, PartnerForm) {
    var UserProfileView = Backbone.View.extend({
        tagName: 'div',
        className: 'reveal-modal modalOrange w450',
        events: {
            'click .nav-tab-item': 'onNavTabClick',
            'click #submit_button': 'formSubmit'
        },
        isModal: true,
        render: function () {
            //wait model sync
            if (!this.model.id) {
                this.model.once('sync', this.render, this);

                return false;
            }

            var html = Twig.render(user_profile, {
                    user: this.model.toJSON()
                }),
                self = this;

            this.form = new PartnerForm({
                model: this.model,
                schema: this.getSchema(),
                fieldsets: this.getFieldsets()
            }).render();

            this.prepareFormTabs();

            this.$el
                .html(html)
                .appendTo($('body'))
                .find('#user_form')
                .append(this.form.el);

            this.createPopup();
            this.refreshStyler();

            return this;
        },
        getSchema: function () {
            var schema = {
                username: {editorClass: 'field-while', type: 'Text', validators: ['required']},
                email: {editorClass: 'field-while', type: 'Text', validators: ['required', 'email']},
                firstname: {editorClass: 'field-while', type: 'Text', validators: ['required']},
                lastname: {editorClass: 'field-while', type: 'Text', validators: ['required']},
                cur_password: {editorClass: 'field-while', type: 'ePassword', validators: ['required']},
                plainPassword: {editorClass: 'field-while', type: 'ePassword', validators: ['required']}
            };

            return schema;
        },
        getFieldsets: function () {
            var fieldsets = [
                {legend: 'мои данные', fields: ['username', 'email', 'firstname', 'lastname']},
                {legend: 'смена пароля', fields: ['cur_password', 'plainPassword']}
//                {legend: 'реквизиты', fields: []}
            ];

            return fieldsets;
        }
    });

    _.extend(UserProfileView.prototype, FormMixin, PopupMixin);

    return UserProfileView;
});