define([
    'jquery-pkg',
    'underscore',
    'backbone-pkg',
    'router',
    'collections/site',
    'views/collection/site',
    'models/user',
    'collections/ticket.subject',
    'collections/ticket'
], function ($, _, Backbone, Router, SiteCollection, SiteCollectionView, UserModel, TicketSubjectCollection) {
    var AppView = Backbone.View.extend({
        el: $('body'),
        events: {
        },
        initialize: function () {
            var siteCollectionView, self = this;

            this.sites = new SiteCollection();
            siteCollectionView = new SiteCollectionView({
                collection: this.sites
            });
            this.ticketSubjects = TicketSubjectCollection;
            this.user = new UserModel();
            this.sites.once('reset', function (collection, options) {
                var app_router = new Router({
                    user: self.user,
                    sites: self.sites,
                    ticketSubjects: self.ticketSubjects
                });
                app_router.bindLinks();
                Backbone.history.start({pushState: true});
            });
        }
    });

    return (new AppView());
});