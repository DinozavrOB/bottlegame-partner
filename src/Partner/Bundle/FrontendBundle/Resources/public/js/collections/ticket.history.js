/**
 * Created by shatun on 11/1/13.
 */
define(['jquery-pkg',
    'underscore',
    'backbone-pkg',
    'models/relational/ticket.history'
], function ($, _, Backbone, TicketHistoryRelationalModel) {
    var TicketHistoryCollection = Backbone.Collection.extend({
        model: TicketHistoryRelationalModel,
        url: function () {
            return Routing.generate('partner_frontend_ticket_history_all', {ticket_id: this.ticket.id});
        }
    });

    return TicketHistoryCollection;
});