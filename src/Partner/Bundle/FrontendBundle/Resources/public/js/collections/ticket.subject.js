define([
    'jquery-pkg',
    'underscore',
    'backbone-pkg',
    'models/relational/ticket.subject'
], function ($, _, Backbone, TicketSubjectRealationalModel) {
    var TicketSubjectCollection = Backbone.Collection.extend({
        model: TicketSubjectRealationalModel,
        url: Routing.generate('partner_frontend_ticket_subject_all'),
        initialize: function () {
            this.fetch({reset: true});
        }
    });

    return (new TicketSubjectCollection());
});
