define([
    'jquery',
    'underscore',
    'backbone-pkg',
    'models/relational/site',
    'views/site'
], function ($, _, Backbone, SiteRelationalModel, SiteView) {
    var SiteCollection = Backbone.Collection.extend({
        model: SiteRelationalModel,
        url: Routing.generate('partner_frontend_site_all'),
        initialize: function () {
            this.fetch({
                reset: true,
                error: function (collection, response, options) {
                    window.notifications.trigger('error', 'не удалось загрузить коллекцию проектов');
                }
            });
        }
    });

    return SiteCollection;
});