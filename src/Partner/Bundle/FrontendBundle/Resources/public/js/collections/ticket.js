/**
 * Created by shatun on 11/1/13.
 */
define(['jquery-pkg',
    'underscore',
    'backbone-pkg',
    'models/relational/ticket'
], function ($, _, Backbone, TicketRelationalModel) {
    var TicketCollection = Backbone.Collection.extend({
        model: TicketRelationalModel,
        url: function () {
            return Routing.generate('partner_frontend_ticket_all');
        }
    });

    return TicketCollection;
});