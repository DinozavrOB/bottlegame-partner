define([
    'jquery',
    'underscore',
    'backbone',
    'models/relational/siteapp'
], function ($, _, Backbone, SiteappRelationalModel) {
    var SiteappCollection = Backbone.Collection.extend({
        model: SiteappRelationalModel,
        url: function () {
            return Routing.generate('partner_frontend_site_get_siteapps', {site_id: this.site.id});
        }
    });

    return SiteappCollection;
})


