/**
 * Created by shatun on 11/7/13.
 */
define([
    'jquery',
    'underscore',
    'backbone-pkg',
    'models/popup'
], function ($, _, Backbone, PopupModel) {
    var PopupCollection = Backbone.Collection.extend({
        model: PopupModel,
        defaultZindex: 97,
        initialize: function () {
            this.on('add', this.onPopupAdd, this);
            this.on('remove', this.onPopupClose, this);
        },
        onPopupAdd: function (model) {
            if (this.length == 1) {
                model.set('zindex', this.defaultZindex);
            } else {
                this.changeZindex();
            }
        },
        onPopupClose: function (model, options) {
            model.get('view').remove();
            if (!this.length) {
                Backbone.history.navigate('', {trigger: false});
                return;
            }

            this.changeZindex();
            var fragment = this.at(this.length - 1).id;
            Backbone.history.navigate(fragment, {trigger: false});
        },
        changeZindex: function () {
            var zindex = this.defaultZindex;
            this.each(function (model) {
                model.set('zindex', zindex);
                zindex++;
            });
        },
        moveToTop: function (id) {
            var model = this.get(id);

            if (_.isEmpty(model)) {
                return false;
            }

            this.remove(model, {silent: true});
            this.push(model);
            Backbone.history.navigate(model.id, {trigger: false});
        },
        hasPopup: function (id) {
            if (this.get(id)) {
                return true;
            }

            return false;
        }
    });

    return (new PopupCollection());
});