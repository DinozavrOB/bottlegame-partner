<?php
/**
 * Created by PhpStorm.
 * User: shatun
 * Date: 11/1/13
 * Time: 1:35 PM
 */

namespace Partner\Bundle\FrontendBundle\Controller;


use FOS\RestBundle\Controller\FOSRestController;
use Partner\Bundle\DataBundle\Entity\Ticket;
use Partner\Bundle\FrontendBundle\Form\Type\TicketFormType;
use Partner\Bundle\UserBundle\Entity\User;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class TicketController extends FOSRestController
{
    public function allAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return $this->forward('PartnerFrontendBundle:Index:index');
        }

        if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
            $em = $this->getDoctrine()->getManager();
            $tickets = $em->getRepository('PartnerDataBundle:Ticket')->findAll();
        } else {
            /* @var $user User */
            $user = $this->getUser();
            $tickets = $user->getTickets();
        }

        $view = $this
            ->view($tickets, 200)
            ->setFormat('json');

        return $this->handleView($view);
    }

    public function getAction(Ticket $ticket)
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return $this->forward('PartnerFrontendBundle:Index:index');
        }

        $this->checkSecurity($ticket);

        $view = $this
            ->view($ticket, 200)
            ->setFormat('json');

        return $this->handleView($view);
    }

    public function newAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return $this->forward('PartnerFrontendBundle:Index:index');
        }

        $ticket = new Ticket();
        $ticket
            ->setAuthor($this->getUser())
            ->setStatus(Ticket::OPEN);

        $view = $this->processForm($ticket);

        return $this->handleView($view);
    }

    public function editAction(Ticket $ticket)
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return $this->forward('PartnerFrontendBundle:Index:index');
        }

        $this->checkSecurity($ticket);

        $view = $this->processForm($ticket);

        return $this->handleView($view);
    }

    protected function processForm(Ticket $ticket)
    {
        $statusCode = $ticket->isNew() ? 201 : 204;
        $form = $this->createForm(new TicketFormType(), $ticket, ['editable' => !$ticket->isNew()]);
        $data = $this->getRequest()->request->all();
        $children = $form->all();
        $data = array_intersect_key($data, $children);
        $form->submit($data, $ticket->isNew());
        $view = $this->view();

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($ticket);
            $em->flush();

            if ($statusCode === 201) {
                $view
                    ->setData($ticket)
                    ->setHeader(
                        'Location',
                        $this->generateUrl('partner_frontend_ticket_get', array('id' => $ticket->getId()))
                    );
            }
        } else {
            $statusCode = 400;
            $view->setData($form);
        }

        $view
            ->setFormat('json')
            ->setStatusCode($statusCode);

        return $view;
    }

    /**
     * @param Ticket $ticket
     * @return bool
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    protected function checkSecurity(Ticket $ticket)
    {
        if (!$this->get('security.context')->isGranted('ROLE_ADMIN') &&
            $ticket->getAuthor()->getId() !== $this->getUser() - getId()
        ) {
            throw new AccessDeniedException();
        }

        return true;
    }

} 