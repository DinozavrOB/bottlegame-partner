<?php
/**
 * Created by PhpStorm.
 * User: shatun
 * Date: 11/5/13
 * Time: 7:10 PM
 */

namespace Partner\Bundle\FrontendBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SupportController extends Controller
{
    /**
     * only for route
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->forward('PartnerFrontendBundle:Index:index');
    }
}