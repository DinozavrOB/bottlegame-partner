<?php

namespace Partner\Bundle\FrontendBundle\Controller;

use Doctrine\Common\Collections\Criteria;
use FOS\RestBundle\Controller\FOSRestController;
use Partner\Bundle\BottlegameApiBundle\Service\BottlegameApi;
use Partner\Bundle\DataBundle\Entity\Site;
use Partner\Bundle\DataBundle\Entity\SiteApp;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Description of SiteappConteroller
 *
 * @author shatun
 */
class SiteappController extends FOSRestController
{
    const REDIS_CACHE_TTL = 3600;

    public function allAction()
    {

    }

    public function getAction($site_id, $id)
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return $this->redirect($this->generateUrl('partner_frontend_homepage'));
        }

        $em = $this->getDoctrine()->getManager();
        /* @var $siteApp SiteApp */
        $siteApp = $em->getRepository('PartnerDataBundle:SiteApp')->findOneById($id);

        if (!$siteApp) {
            throw new NotFoundHttpException('Site application not found');
        }

        $this->checkSecurity($siteApp);

        $view = $this->view($siteApp, 200)
            ->setFormat('json');

        return $this->handleView($view);
    }

    public function editAction($site_id, $id)
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return $this->forward('PartnerFrontendBundle:Index:index');
        }

        throw new NotFoundHttpException('Site application not found');
    }


    /**
     * @ParamConverter("site", class="PartnerDataBundle:Site", options={"id" = "site_id"})
     *
     */
    public function getAppUrlAction(Site $site, $id)
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return $this->forward('PartnerFrontendBundle:Index:index');
        }

        $hash = sprintf('api_cache:appurl:site-%d:app-%d', $site->getId(), $id);

        /* @var $redis \Predis\Client */
        $redis = $this->get('snc_redis.default');

        if ($redis->exists($hash)) {
            $url = $redis->get($hash);
        } else {
            $siteApp = $site->getApps()
                ->matching(
                    Criteria::create()->andWhere(
                        Criteria::expr()->eq('id', $id)
                    )
                )->first();

            if (!$siteApp) {
                throw new NotFoundHttpException();
            }

            $this->checkSecurity($siteApp);

            /* @var $api BottlegameApi */
            $api = $this->get('bottlegame_api.api');
            $url = $api->getAppUrl($siteApp);

            $redis->setex($hash, self::REDIS_CACHE_TTL, $url);
        }

        $view = $this->view(array('apiurl' => $url))
            ->setFormat('json');

        return $this->handleView($view);
    }

    protected function checkSecurity(Siteapp $siteApp)
    {
        if (!$this->get('security.context')->isGranted('ROLE_ADMIN')) {
            $siteUser = $siteApp->getSite()->getUser();
            if ($siteUser->getId() !== $this->getUser()->getId()) {
                throw new AccessDeniedException();
            }
        }
    }
}

?>