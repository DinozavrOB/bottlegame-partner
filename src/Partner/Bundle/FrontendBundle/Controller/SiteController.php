<?php

namespace Partner\Bundle\FrontendBundle\Controller;

use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Controller\FOSRestController;
use Partner\Bundle\DataBundle\Entity\Site;
use Partner\Bundle\DataBundle\Entity\SiteApp;
use Partner\Bundle\FrontendBundle\Form\Type\SiteFormType;
use Partner\Bundle\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SiteController extends FOSRestController
{
    public function allAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return $this->redirect($this->generateUrl('partner_frontend_homepage'));
        }

        if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
            $em = $this->getDoctrine()->getManager();
            $sites = $em->getRepository('PartnerDataBundle:Site')->findAll();
        } else {
            /* @var $user User */
            $user = $this->getUser();
            $sites = $user->getSites();
        }

        $view = $this->view($sites, 200)
            ->setFormat('json');

        $view->getSerializationContext()->enableMaxDepthChecks();

        return $this->handleView($view);
    }

    public function getAction($id)
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return $this->redirect($this->generateUrl('partner_frontend_homepage'));
        }

        $site = $this->getSite($id);

        $view = $this->view($site, 200)
            ->setFormat('json');

        $view->getSerializationContext()->enableMaxDepthChecks();

        return $this->handleView($view);
    }

    public function newAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return $this->forward('PartnerFrontendBundle:Index:index');
        }

        $site = new Site();
        $site->setUser($this->getUser());
        $view = $this->processForm($site);

        if ($view->getStatusCode() !== 400) {

            /* @var $em EntityManager */
            $em = $this->getDoctrine()->getManager();
            $appRepo = $em->getRepository('PartnerDataBundle:Application');
            $aplications = $appRepo->findBy(array('active' => true));

            foreach ($aplications as $app) {
                $siteApp = new SiteApp();
                $siteApp->setApp($app);
                $site->addApp($siteApp);
                $em->persist($siteApp);
            }
            $em->persist($site);

            $em->flush();
        }

        return $this->handleView($view);
    }

    public function editAction(Site $site)
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return $this->forward('PartnerFrontendBundle:Index:index');
        }

        $view = $this->processForm($site);

        return $this->handleView($view);
    }

    public function removeAction(Site $site)
    {

    }

    public function getSiteAppsAction($site_id)
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return $this->redirect($this->generateUrl('partner_frontend_homepage'));
        }

        $site = $this->getSite($site_id);

        $siteApps = $site->getApps();
        $view = $this->view($siteApps, 200)
            ->setFormat('json');

        return $this->handleView($view);
    }

    private function processForm(Site $site)
    {
        $statusCode = $site->isNew() ? 201 : 204;
        $form = $this->createForm(new SiteFormType(), $site);
        $data = $this->getRequest()->request->all();
        $children = $form->all();
        $data = array_intersect_key($data, $children);
        $form->submit($data, $site->isNew());
        $view = $this->view();

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($site);
            $em->flush();

            if ($statusCode === 201) {
                $view
                    ->setData($site)
                    ->setHeader(
                        'Location',
                        $this->generateUrl('partner_frontend_site_get', array('id' => $site->getId()))
                    );
            }

        } else {
            $statusCode = 400;
            $view->setData($form);
        }

        $view
            ->setFormat('json')
            ->setStatusCode($statusCode);

        return $view;
    }

    /**
     *
     * @param $id
     * @return Site
     * @throws NotFoundHttpException
     * @throws AccessDeniedHttpException
     */
    protected function getSite($id)
    {
        $em = $this->getDoctrine()->getManager();
        $siteRepo = $em->getRepository('PartnerDataBundle:Site');
        /* @var $site Site */
        $site = $siteRepo->findOneById($id);

        if (!$site) {
            throw new NotFoundHttpException('site not found');
        }

        if (!$this->get('security.context')->isGranted('ROLE_ADMIN') &&
            $this->getUser()->getId() !== $site->getUser()->getId()
        ) {
            throw new AccessDeniedHttpException();
        }

        return $site;
    }
}

?>