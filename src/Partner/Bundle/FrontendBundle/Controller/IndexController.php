<?php

namespace Partner\Bundle\FrontendBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Partner\Bundle\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class IndexController extends Controller
{
    /**
     * @Template
     */
    public function indexAction()
    {
        if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
            $em = $this->getDoctrine()->getManager();
            $result = $em->getRepository('PartnerDataBundle:Site')->findAll();
            $sites = new ArrayCollection($result);
        } else {
            /* @var $user User */
            $user = $this->getUser();
            $sites = $user->getSites();
        }

        return ['sites' => $sites];
    }
}
