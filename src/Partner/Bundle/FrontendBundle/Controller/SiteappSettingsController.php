<?php

namespace Partner\Bundle\FrontendBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Partner\Bundle\DataBundle\Entity\Setting;
use Partner\Bundle\DataBundle\Entity\SettingGroup;
use Partner\Bundle\DataBundle\Entity\SiteApp;
use Partner\Bundle\FrontendBundle\Form\Type\SiteappSettingsFormType;

class SiteappSettingsController extends FOSRestController
{

    public function getAction(SiteApp $siteApp)
    {
        $siteappSettings = $siteApp->getSettings();
        $appSettings = $siteApp->getApp()->getSettings()->toArray();
        $data = array(
            'siteapp_settings' => $siteappSettings,
            'settings' => $this->groupAppSettings($appSettings)
        );

        $view = $this->view($data, 200)
            ->setFormat('json');

        return $this->handleView($view);
    }

    public function editAction(SiteApp $siteApp)
    {
        $form = $this->createForm(new SiteappSettingsFormType($siteApp), $siteApp);
        $data = $this->getRequest()->request->all();
        $submittedData = array('settings' => $data);
        $form->submit($submittedData, false);

        $view = $this->view();
        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $view->setStatusCode(201)
                ->setData($siteApp->getSettings());
        } else {
            $view
                ->setStatusCode(400)
                ->setData($form);
        }

        $view->setFormat('json');

        return $this->handleView($view);
    }

    protected function groupAppSettings($appSettings)
    {
        $groups = $groupSettings = array();

        /* @var $setting Setting */
        foreach ($appSettings as $setting) {
            $group = $setting->getGroup();
            if ($group instanceof SettingGroup && !array_key_exists($group->getId(), $groups)) {
                $groups[$group->getId()] = $group;
            }
        }

        usort(
            $groups,
            function ($a, $b) {
                if ($a->getSort() === $b->getSort()) {
                    return 0;
                }

                return ($a->getSort() < $b->getSort()) ? -1 : 1;
            }
        );

        /* @var $group SettingGroup */
        foreach ($groups as $group) {
            $groupSettings[] = array(
                'group' => $group,
                'settings' => array_filter(
                    $appSettings,
                    function ($val) use ($group) {
                        return $val->getGroup() == $group;
                    }
                )
            );
        }

        return $groupSettings;
    }

}

?>