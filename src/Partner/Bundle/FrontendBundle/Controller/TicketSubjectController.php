<?php
/**
 * Created by PhpStorm.
 * User: shatun
 * Date: 11/5/13
 * Time: 5:52 PM
 */

namespace Partner\Bundle\FrontendBundle\Controller;


use FOS\RestBundle\Controller\FOSRestController;
use Partner\Bundle\DataBundle\Entity\TicketSubject;

class TicketSubjectController extends FOSRestController
{
    public function allAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return $this->redirect($this->generateUrl('partner_frontend_homepage'));
        }

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('PartnerDataBundle:TicketSubject');

        $ticketSubjects = $repo->findBy(array('active' => true));
        $view = $this->view($ticketSubjects, 200)
            ->setFormat('json');

        return $this->handleView($view);
    }

    public function getAction(TicketSubject $ticketSubject)
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return $this->redirect($this->generateUrl('partner_frontend_homepage'));
        }

        $view = $this->view($ticketSubject, 200)
            ->setFormat('json');

        return $this->handleView($view);
    }

}