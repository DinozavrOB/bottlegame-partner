<?php
/**
 * Created by PhpStorm.
 * User: shatun
 * Date: 11/1/13
 * Time: 1:46 PM
 */

namespace Partner\Bundle\FrontendBundle\Controller;


use Doctrine\Common\Collections\Criteria;
use FOS\RestBundle\Controller\FOSRestController;
use Partner\Bundle\DataBundle\Entity\Ticket;
use Partner\Bundle\DataBundle\Entity\TicketHistory;
use Partner\Bundle\FrontendBundle\Form\Type\TicketHistoryFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class TicketHistoryController extends FOSRestController
{

    /**
     * @ParamConverter("ticket", class="PartnerDataBundle:Ticket", options={"id" = "ticket_id"})
     *
     */
    public function allAction(Ticket $ticket)
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return $this->redirect($this->generateUrl('partner_frontend_homepage'));
        }

        $this->checkSecurity($ticket);

        $history = $ticket->getHistory();

        $view = $this
            ->view($history, 200)
            ->setFormat('json');

        return $this->handleView($view);
    }

    /**
     * @ParamConverter("ticket", class="PartnerDataBundle:Ticket", options={"id" = "ticket_id"})
     *
     */
    public function getAction(Ticket $ticket, $id)
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return $this->redirect($this->generateUrl('partner_frontend_homepage'));
        }

        $this->checkSecurity($ticket);

        $historyItem = $ticket
            ->getHistory()
            ->matching(
                Criteria::create()->where(
                    Criteria::expr()->eq('id', $id)
                )
            );

        /* @var $historyItem TicketHistory */
        if (!$historyItem) {
            throw new NotFoundHttpException();
        }

        $view = $this
            ->view($historyItem, 200)
            ->setFormat('json');

        return $this->handleView($view);
    }

    /**
     * @ParamConverter("ticket", class="PartnerDataBundle:Ticket", options={"id" = "ticket_id"})
     *
     */
    public function newAction(Ticket $ticket)
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return $this->forward('PartnerFrontendBundle:Index:index');
        }

        $this->checkSecurity($ticket);

        $history = new TicketHistory();
        $history->setTicket($ticket)
            ->setAuthor($this->getUser());

        $view = $this->processForm($history);

        return $this->handleView($view);
    }

    protected function processForm(TicketHistory $history)
    {
        $statusCode = $history->isNew() ? 201 : 204;
        $form = $this->createForm(new TicketHistoryFormType(), $history);
        $data = $this->getRequest()->request->all();
        $children = $form->all();
        $data = array_intersect_key($data, $children);
        $form->submit($data, $history->isNew());
        $view = $this->view();

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($history);
            $em->flush();

            if ($statusCode === 201) {
                $view
                    ->setData($history)
                    ->setHeader(
                        'Location',
                        $this->generateUrl(
                            'partner_frontend_ticket_history_get',
                            array(
                                'id' => $history->getId(),
                                'ticket_id' => $history->getTicket()->getId()
                            )
                        )
                    );
            }
        } else {
            $statusCode = 400;
            $view->setData($form);
        }

        $view
            ->setFormat('json')
            ->setStatusCode($statusCode);

        return $view;
    }

    /**
     * @param Ticket $ticket
     * @return bool
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    protected function checkSecurity(Ticket $ticket)
    {
        if (!$this->get('security.context')->isGranted('ROLE_ADMIN') &&
            $ticket->getAuthor()->getId() !== $this->getUser() - getId()
        ) {
            throw new AccessDeniedException();
        }

        return true;
    }
} 