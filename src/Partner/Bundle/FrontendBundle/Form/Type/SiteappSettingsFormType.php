<?php

namespace Partner\Bundle\FrontendBundle\Form\Type;

use Doctrine\Common\Collections\Criteria;
use Partner\Bundle\DataBundle\Entity\Setting;
use Partner\Bundle\DataBundle\Entity\SiteApp;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SiteappSettingsFormType extends AbstractType
{
    /**
     *
     * @var SiteApp
     */
    protected $siteapp;

    public function __construct(Siteapp $siteapp)
    {
        $this->siteapp = $siteapp;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $settings = $this->siteapp->getApp()->getSettings();
        $settings->initialize();
        $settings->matching(
            Criteria::create()->where(
                Criteria::expr()->eq('editable', true)
            )
        );


        $settingsForm = $builder->getFormFactory()->createNamedBuilder('settings');

        /* @var $setting Setting */
        foreach ($settings as $setting) {
            $type = $setting->getType();
            switch ($type) :
                case 'string':
                    $settingsForm->add($setting->getSlug(), 'text');
                    break;
                case 'boolean':
                    $settingsForm->add($setting->getSlug(), 'text');
                    break;
                case 'array':
                    $settingsForm->add(
                        $setting->getSlug(),
                        'choice',
                        array('choices' => $setting->getAvailableValuesChoices())
                    );
                    break;
            endswitch;
        }

        $builder->add($settingsForm);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Partner\Bundle\DataBundle\Entity\SiteApp',
                'csrf_protection' => false
            )
        );
    }

    public function getName()
    {
        return 'settings';
    }
}

?>