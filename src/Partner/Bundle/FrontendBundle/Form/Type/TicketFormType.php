<?php
/**
 * Created by PhpStorm.
 * User: shatun
 * Date: 11/1/13
 * Time: 2:41 PM
 */

namespace Partner\Bundle\FrontendBundle\Form\Type;


use Partner\Bundle\DataBundle\Entity\Ticket;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TicketFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['editable']) {
            $builder->add('status');

            return;
        }

        $builder
            ->add('subject')
            ->add('description');

        $data = $builder->getData();

        if (!$data instanceof Ticket) {
            return;
        }

        if (!$data->getSite()) {
            $builder->add('site');
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setOptional(array('edit'))
            ->setAllowedValues(array('edit' => array(true, false)))
            ->setDefaults(
                array(
                    'data_class' => 'Partner\Bundle\DataBundle\Entity\Ticket',
                    'csrf_protection' => false,
                    'editable' => false
                )
            );
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'ticket';
    }

} 