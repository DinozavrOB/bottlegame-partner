<?php
/**
 * Created by PhpStorm.
 * User: shatun
 * Date: 11/9/13
 * Time: 3:59 PM
 */

namespace Partner\Bundle\FrontendBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TicketHistoryFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('message');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Partner\Bundle\DataBundle\Entity\TicketHistory',
                'csrf_protection' => false
            )
        );
    }


    public function getName()
    {
        return 'history';
    }
}