<?php

namespace Partner\Bundle\FrontendBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SiteFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, array('attr' => ['class' => 'field-while']))
            ->add('visits', null, array('attr' => ['class' => 'field-while']))
            ->add('link', null, array('attr' => ['class' => 'field-while']))
            ->add('status');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Partner\Bundle\DataBundle\Entity\Site',
                'csrf_protection' => false
            )
        );
    }

    public function getName()
    {
        return 'site';
    }
}

?>