<?php

namespace Partner\Bundle\FrontendBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        /* @var $router \Symfony\Bundle\FrameworkBundle\Routing\Router */
        $router = $this->container->get('router');

        $menu->addChild('my', array('label' => 'Мои данные', 'uri' => $router->generate('partner_frontend_my_get')));
//        $menu->addChild('stat', array('label'=>'Статистика', 'uri'=>$router->generate('partner_frontend_stat')));
//        $menu->addChild('payment', array('label'=>'Выплаты', 'uri'=>$router->generate('partner_frontend_payment')));
        $menu->addChild(
            'support',
            array('label' => 'Поддержка', 'uri' => $router->generate('partner_frontend_support'))
        );
        $menu->addChild('logout', array('label' => 'Выйти', 'uri' => '/logout'));

        $menu['logout']
            ->setLinkAttributes(array('class' => 'red button', 'data-bypass' => true))
            ->setAttribute('class', 'padding_rl_24');

        $menu->setChildrenAttribute('class', 'header-nav clear');

        return $menu;
    }
}

?>
